<?php get_header(); ?>
<?php //the_post(); ?>

    
    <div class="apartment-alarm">
        <div class="page-title"><?php the_title(); ?></div>
        <div class="line-27"></div>
        <div class="container-1470">

            <?php $coo_elements = get_field('block_coord_title_elements'); ?>
            

            <div class="apartment-alarm-home">
                <div class="apartment-alarm-home_img">
                    <?php if($image_id_coo = get_field('block_coord_image')){ ?>
                        <img src="<?= wp_get_attachment_image_url( @$image_id_coo, 'orland-single_service_img_pos' )?>" alt="">
                    <?php } else { ?>
                        <img src="<?=get_template_directory_uri()?>/orland/build/img/apartment-alarm-home_img.png" alt="">
                    <?php } ?>
                    <?php if($coo_elements){ ?>
                    <?php $coo_i = 1; ?>
                    <?php foreach($coo_elements as $element){ ?>
                        
                        <span style="top: <?=$element['y']?>%;left: <?=$element['x']?>%;" class="num-<?=$coo_i?>" data-toggle="tooltip" data-placement="right" title="<?=__($element['content'])?>"><?=$coo_i?></span>
                        
                    <?php $coo_i++; } ?>
                    <?php } ?>

                </div>
                
                <div class="apartment-alarm-home_info">
                    <?php if($coo_elements){ ?>
                    <p><?php the_field('block_coord_title') ?></p>
                    <ul>
                    <?php $coo_i = 1; ?>
                    <?php foreach($coo_elements as $element){ ?>
                        <li><span data-toggle="tooltip" data-href="num-<?=$coo_i?>" data-placement="right" title="<?=__($element['content'])?>"><?=$coo_i?></span><?=__($element['title'])?></li>
                        <?php $coo_i++; } ?>
                    </ul>
                    <?php } ?>
                </div>
                
            </div>
            
            
            <div class="apartment-alarm-block-info">
            <?php $info_block = get_field('info_block'); ?>
            <?php if($info_block){ ?>
                <div class="apartment-alarm-description">
                    <ul>
                        <?php foreach($info_block as $element){ ?>
                        <li>
                            <img src="<?=wp_get_attachment_image_url( @$element['img'], 'orland-gallery' )?>" alt="">
                            <div class="apartment-alarm-description-name"><?=__($element['title'])?></div>
                            <p>
                                <?=__($element['content'])?>
                            </p>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <?php } ?>
                
                <div class="apartment-alarm-info">
                <?php $info_block_2 = get_field('info_block_2'); ?>
                <?php if($info_block_2){ ?>
                    <ul>
                        <?php foreach($info_block_2 as $element){ ?>
                        <li>
                            <div class="apartment-alarm-info-num"><span><?=__($element['qty'])?></span>+</div>
                            <p><?=__($element['content'])?></p>
                        </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                </div>
                
            </div>
        </div>
    </div>
    <?php $action_block = get_field('action_block'); ?>
    <?php if($action_block){ ?>
    <?php foreach($action_block as $action){ ?>
    <div class="ap_al-baners">
        <div class="ap_al-baners-bg">
            <img src="<?=wp_get_attachment_image_url( @$action['image'], 'orland-single_service_img_pos' )?>" alt="">
        </div>
        <div class="container-1470">
            <div class="ap_al-baner-package">
                <div class="ap_al-baner-package-name"><?=__($action['title'])?></div>
                <p><?=__($action['content'])?></p>
                <a data-toggle="modal" href="<?=$action['href']?>"><?php the_field('lng_get','option') ?></a>
            </div>
            <div class="ap_al-baner-question">
                <div class="ap_al-baner-question-name"><?=__($action['text_1'])?></div>
                <a data-toggle="modal" href="<?=$action['href_2']?>"><?=__($action['text_2'])?> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="33" height="15" viewBox="0 0 33 15"><g><g transform="translate(-936 -1142)"><image width="33" height="15" transform="translate(936 1142)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAPCAYAAABqQqYpAAABuElEQVRIS63VT4iNYRTH8c9g1PjTZCGLURNbTcrGFIaMhczC3sJCYWwUEks7FlJmJoqYqSFiJv9WlAULzMJCYWFDsRBrymLQqXPr7fbe633HPXV73u49z3O+95zf73m71ItVuIHl9ba1zF6M110lP69BT9P33/AT59CHd/n8PyzbcR6/yyBOZqFigVuYw3PsxlW8xMQCKJbgDDbiMO6UQbQ6txtPMYRo4008w+UaIOtxHfcwliCjdSA24SCOZNGluIsZTFcA2Yej+e/fZP4hzNeBGMUvTBYKBsgsbmdnylhW4hJ+4Hhqq5EXY70YEBsQOmgVoYfHuIYLKcpi7jI8SH3EWozNGMfZHEFzjRfYFhAhlLVtIBrOCCFuCTWX5AbIw5xzrItwGjtwAF9K9oTNH2Fn1XGswH3sagPbmzlvMYAnaeMy6DhmK/bGFKpChKf34NQ/BBiuCYCv+WmXfgKfyyx6DP2FnVfwPjXzMZ1QwQiVUkLMMbJPzZ1Yh2hrIz6kmsOKoewg71S8wmAcVnUccVuG0jsVqzGFkToQcVMOd4ogL73oeLw7KnciqGNUfzoAEt3/jv2Ni+svznNSV6oP7mwAAAAASUVORK5CYII="></image></g></g></svg></a>
            </div>
        </div>
    </div>
    <?php } } ?>
    <?php if(0){ ?>
    <!-- How the system works (Как работает система) -->
    <?php $block_system_elements = get_field('block_system_elements'); ?>
    <?php if($block_system_elements){ ?>
    <div class="ht-system">
        <div class="page-title"><?php the_field('block_system_title') ?></div>
        <div class="line-27"></div>
        <div class="container-1470">
            <ul class="ht-system-list">
                <?php $key_elem = 1; ?>
                <?php foreach($block_system_elements as $element){ ?>
                <li><span><?=$key_elem?></span><p><?=__(@$element['title'])?></p></li>
                <?php $key_elem++; } ?>
            </ul>
        </div>
    </div>
    <?php } ?>
    <?php } ?>
    <!-- Advantages  of Orlan (Приемущества компании Орлан) -->
    <?php $block_advantages_elements = get_field('block_advantages_elements'); ?>
    <?php if($block_advantages_elements){ ?>
    <div class="advantages-company">
        <div class="page-title"><?php the_field('block_advantages_title') ?></div>
        <div class="line-27"></div>
        <div class="container-1470">
            <ul class="advantages-company-list">
                <?php foreach($block_advantages_elements as $element){ ?>
                <li><img src="<?=wp_get_attachment_image_url( @$element['image'], 'orland-gallery' )?>" alt=""><p><?=__(@$element['title'])?></p></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <?php } ?>

    
    <?php $block_operating_elements = get_field('block_operating_elements'); ?>
    <?php if($block_operating_elements){ ?>
    <?php $img_bg_operating = wp_get_attachment_image_url( get_field('block_operating_image'), 'full_shuviha' ); ?>
    <!-- Operating procedure (Порядок работы) -->
    <div class="operating-procedure" >
        <div class="operating-procedure-bg">
            <?php if($img_bg_operating){ ?>
                <img src="<?=$img_bg_operating?>" alt="">
            <?php } ?>
        </div>
        <div class="container-1470">
            <div class="operating-procedure-medal">
                <p><?php the_field('block_operating_content') ?></p>
            </div>
            <div class="operating-procedure-content">
                <div class="operating-procedure-name"><i class="icon-newspaper"></i><?php the_field('block_operating_title') ?></div>
                <div class="operating-procedure-box">
                    <ul>
                        <?php foreach($block_operating_elements as $element){ ?>
                        <li><?=__(@$element['title'])?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <!-- Alarm in the apartment (Сигнализация в квартиру) -->
    <div class="alarm-apartment">
        <div class="page-title"><?php the_field('title_2') ?></div>
        <div class="line-27"></div>
        <div class="content-1470">
            <div class="alarm-apartment-text">
               <?php the_content(); ?>
            </div>
            <div class="alarm-apartment-open">
                <span class="text-open">
                    <?php the_field('lng_opn','option') ?>
                </span>
                <span class="text-close">
                    <?php the_field('lng_clos','option') ?>
                </span>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15" height="13" viewBox="0 0 15 13"><g><g transform="translate(-812 -3617)"><image width="15" height="13" transform="translate(812 3617)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAANCAYAAAB2HjRBAAABuklEQVQ4T41SMWiTURC+e+EvGUoCIoKDe2sKoZCGe48sGSxFpw4igosOoqMtdClCsaJTi9BOFezSpQiORaSQ6X/3EjNoJcVdXMQlsUMSeP+VV/qH31jR297d99377rtDZv6BiPeI6AP8ZzSbzVtJkuxiHMdzSqlDALivtX73L7619rZSasd7P48BbK2dQkQnIkvGmDd/a2CtfaCU2kiSRBtjvp6RQ7RarWve+0+I+IKINsYbOOeWRWQ1l8uVq9Xqt1AfkcMjjuMrSqkvALCjtX6aNmDm5wDwMIqi65VK5WeaR2ZezwKttZcQ8QgR3xLRE2beAoBF7/10rVb7lRKttc/QOWdDgohMWmg0GpP5fD4o6IvIxGAwKNfr9ZOMEgYAfybbOfdeRK4S0SwiJiHX6XQmut3uZrFYXCqVSsOQExHlnPuMiN+JaGE0MzPvA8BsoVCYScFZ00KzXq93DAAftdZ3/zCMmV8DwI1+vz+TlXk+xjEiHhDRo5Fh4yth5k0AuBNFUTk42263Lw+HwyOl1B4RrWTxv60qY8gaADxGxJsicoCI20S0Pv7RheRzE8NRvBSRFWPMq4uu7hTN7bu+Ifde/AAAAABJRU5ErkJggg=="/></g></g></svg>
            </div>
        </div>
    </div>

<script>
    var $ = jQuery;
$( document ).ready(function() {

    var show = true;
    var countbox = ".apartment-alarm-info";
    $(window).on("scroll load resize", function(){
        if(!show) return false;                   // Отменяем показ анимации, если она уже была выполнена
        var w_top = $(window).scrollTop();        // Количество пикселей на которое была прокручена страница
        var e_top = $(countbox).offset().top;     // Расстояние от блока со счетчиками до верха всего документа
        var w_height = $(window).height();        // Высота окна браузера
        var d_height = $(document).height();      // Высота всего документа
        var e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
        if(w_top + 400 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height){
            $(".apartment-alarm-info-num span").spincrement({
                thousandSeparator: "",
                duration: 1900
            });
            show = false;
        }
    });
});
</script>

<?php get_footer(); ?>
