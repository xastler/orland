<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">
    <meta name="theme-color" content="#fff">
    
    <?php wp_head(); ?>

</head>
<body>
<!-- BEGIN  wrapper-->
<div class="panel-menu">
    <div class="panel-menu-top">
        <div class="panel-menu-close"><i class="icon-close"></i></div>
    </div>
    <div class="panel-menu-list">
        <?php wp_nav_menu( array(
                'theme_location'  => 'left',
                'container'       => 0, 
                'menu_class'      => '', 
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',

            ) );
        ?>
    </div>
</div>
<div class="wrapper">
    <header class="header">
            <div class="header-navigation">
                <div class="header-navigation-btn">
                    <i class="icon-list"></i>
                </div>
                <?php wp_nav_menu( array(
                        'theme_location'  => 'top',
                        'container'       => 0, 
                        'menu_class'      => 'header-navigation-menu', 
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',

                    ) );
                ?>

            </div>
            <div class="header-options">
                <div class="header-options-call_back" data-toggle="modal" data-target="#popups-call-back">
                    <i class="icon-phone-call-5"></i>
                    <p ><?php the_field('request_a_call','option') ?></p>
    
                </div>
                <div class="container-fluid"></div>

                <?php ob_start(); ?>
               
                <?php  qtranxf_generateLanguageSelectCode('custum',$id)?>
                <?php $qtr = ob_get_contents(); ?>
                <?php ob_end_clean(); ?>
                <?php echo str_replace('qtranxs_language_chooser',' qtranxs_language_chooser header-options-language ',$qtr); ?>
                <?php if(false){ ?>
                <ul class="header-options-language">
                    <li class="active"><a href="#">ru</a></li>
                    <li><a href="#">en</a></li>
                </ul>
                <?php } ?>
            </div>
            <div class="header-logo">
                <?php if(is_front_page()){ ?>
                    <img src="<?=get_logo_site_src()?>" alt="logo">
                <?php } else { ?>
                    <a href="<?= get_site_url() ?>"><img src="<?=get_logo_site_src()?>" alt="logo"></a>
                <?php } ?>
            </div>
            <div class="header-info">
                <?php $header_phones = get_field('header_phones','option'); ?>
                <div class="header-phone">
                    <?php if($header_phones){ ?>
                        <?php foreach($header_phones as $header_phone){ ?>
                            <p><a href="tel:<?= str_replace(['-',' ',')','('],'',__($header_phone['pre']).__($header_phone['tel'])); ?>"><span><?php _e($header_phone['pre']); ?> </span><?php _e($header_phone['tel']); ?></a></p>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="header-application">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15" height="15" viewBox="0 0 15 15"><g><g transform="translate(-1406 -162)"><image width="15" height="15" transform="translate(1406 162)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAA7ElEQVQ4T5XTsS6EQRQG0PN1Kp1EJAreQKGh8QQqtbdQKAgVhYhGFBoi0es0EolYBRKRqKgVFAoPMDKb3WSL9f+70849c2++mYkxVyllCod4yzi2B69xhP2UUiawjpmWg56xg71e/V3FV6gbrw14EhvY7cFOkm7nxySL/8GBUfsdu7DWV3ybZGUYHgI/sZnkpxEP64hpnCV5acPnuMEauqOWUmrKzbiUUpN/xwcukhzUTqPiBaziJMlXP4+RcEPy7WOPg++TLI/yTEspxzgdTLuDbTy1HDCLSywl+e1f1Ty2MNeCv+tvSvLQr/sDSRqL2c2a6tcAAAAASUVORK5CYII="/></g></g></svg>
                    <p data-toggle="modal" data-target="#popups-application"><?php the_field('submit_your_application','option') ?></p>
                </div>
            </div>
    </header>
<?php $tax_slug = false; ?>
<?php if(is_front_page()){ ?>
    
    <div class="home-slider">
        <div class="one-sliders">

            <?php $home_sliders = get_field('home_slider'); ?>
            <?php if($home_sliders){ ?>
            <?php foreach($home_sliders as $slider){ ?>

                <div class="one-slider">
                    <div class="one-slider-img">
                        <span></span>
                        <img src="<?= wp_get_attachment_image_url( @$slider['img'], 'orland-home-slider' )?>" alt="slider">
                    </div>
                    <div class="container-content">
                        <div class="one-slider-info">
                            <div class="page-title"><?=__($slider['title'])?> <i><img src="<?=get_template_directory_uri()?>/orland/build/img/remote-control-buttons.png" alt=""></i></div>
                            <p><?=__($slider['content'])?></p>
                            <a href="<?=$slider['more']?>"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="7" viewBox="0 0 13 7"><g><g transform="translate(-107 -441)"><image width="13" height="7" transform="translate(107 441)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAHCAYAAADTcMcaAAAAXklEQVQoU2NkIAC0tbV5ODg4drCwsEScPHnyCUg5IyFNIHkTExO/////9////9/93Llzd4jSBNJoZGTkw8jI2Pf//38vRmNj4wfE2AZVI8bAwHCJPJuIsYVkP2ELPQC1ZSvd0s8aPwAAAABJRU5ErkJggg=="/></g></g></svg></span><?php the_field('get_more','option') ?></a>
                        </div>
                    </div>
                </div>

                
            <?php } ?>
            <?php } ?>
        </div>
        <div class="one-sliders-num">
            <p><span>01</span> / 0<?=count($home_sliders)?></p>
        </div>
    </div>
    
<?php } else { ?>
    
    
            
        <?php if(is_tax() or is_category() or is_single()){ ?>
            
            <?php

            $post_type = get_post_type(  get_the_ID() );

            
            
            if(!(is_tax() or is_category())){
                
                if($post_type == 'service'){
                    $cats = get_the_terms( get_the_ID(), 'services' );
                    $current_cat_ID = @$cats[0]->term_id;

                    $get_cat_name = __($cats[0]->name);
                    $get_cat_description = __($cats[0]->description);

          //        echo '<pre>';
            //    var_dump($cats);

                $tax_slug = @$cats[0]->slug;
                
                } else {
                    $category = get_the_category(get_the_ID());
                    $current_cat_ID = @$category[0]->cat_ID;

                    $get_cat_name = __(get_cat_name($current_cat_ID));
                    
                }

            } else {
                $current_cat_ID = get_query_var('cat');

                $category = get_the_category($current_cat_ID);

                if(!$category){
                    global $wp_query;

                    if(isset($wp_query->queried_object->term_id)){
                        $term = get_term($wp_query->queried_object->term_id);

                        $get_cat_name = __($term->name);
                        $current_cat_ID = $wp_query->queried_object->term_id;
                        $get_cat_description = __($term->description);

                        $tax_slug = @$term->slug;

                        //var_dump($term); echo '-'; 
                    }
                } else {
                    $get_cat_name = __(get_cat_name($current_cat_ID));
                    $get_cat_description = __('');
                }

            }

            
            ?>

            <div class="title-img price-list <?php if($post_type == 'service'){ ?>panel-protection<?php } ?>">

            <?php
            
            $image_baner = wp_get_attachment_image_url( get_field('image_baner','category_'.$current_cat_ID), 'orland-baner_page' );
            if(!$image_baner){
                $image_baner = get_template_directory_uri().'/orland/build/img/bg-page-promotions.png';
            }
            ?>
            <img src="<?=$image_baner?>" alt="">
            <div class="title-img-text">
                <?php if($post_type == 'service'){ ?>
                     <h1><?= $get_cat_name; ?></h1>
                     <p><?=$get_cat_description?></p>

                     <?php // var_dump($current_cat_ID); exit; ?>

                <?php $banner_header_icon = get_field('banner_header_icon','services_'.$current_cat_ID); ?>
                <?php if(($banner_header_icon) and ($banner_header_icon != '0')){ ?>
                    <i class="icon-<?=$banner_header_icon?>"></i>
                <?php } else { ?>
                
                <?php } ?>
            
                <?php } else { ?>
                    <h1><?= $get_cat_name; ?>

                    <?php $banner_header_icon = get_field('banner_header_icon','category_'.$current_cat_ID); ?>
                <?php if(($banner_header_icon) and ($banner_header_icon != '0')){ ?>
                    <i class="icon-<?=$banner_header_icon?>"></i>
                <?php } else { ?>
                
                <?php } ?>


                 </h1>
                <?php } ?>
                
                  
            </div>
        <?php } else { ?>
            <div class="title-img price-list ">
            <?php



            $image_baner = get_template_directory_uri().'/orland/build/img/bg-page-promotions.png';

            $banner_pages = get_field('banner_pages',get_the_ID());
            if($banner_pages){
                $image_baner = wp_get_attachment_image_url( $banner_pages, 'orland-baner_page' );
            }

             ?>
            
            <img src="<?=$image_baner?>" alt="">
            <div class="title-img-text">
                <h1><?php the_title(); ?>
                <?php $banner_header_icon = get_field('banner_header_icon',get_the_ID()); ?>
                <?php if(($banner_header_icon) and ($banner_header_icon != '0')){ ?>
                    <i class="icon-<?=$banner_header_icon?>"></i>
                <?php } else { ?>
                
                <?php } ?>

                 </h1>
                 
            </div>

        <?php } ?>
        

    </div>
    
<?php } ?>

    <div class="main-menu">
        <ul>
            <li class="<?php if(is_front_page()){ ?>active<?php } ?>"><a href="<?= (is_front_page()) ? '#' : get_site_url() ; ?>"><?php the_field('lng_home','option') ?><i class="icon-web-home"></i></a></li>

            <?php $args = array(
                'taxonomy' => 'services',
                'hide_empty' => false,
                'orderby'       => 'id', 
                'order'         => 'ASC',
            );
            $terms = get_terms( $args );

            //var_dump($terms);

            ?>
            
            <?php foreach($terms as $item){ ?>
                <?php $t_link = get_term_link($item->term_id, 'services'); ?>
                
                <?php $find_slug = false; if($tax_slug){
                    if(strripos($t_link, $tax_slug) !== false){
                         $find_slug = true;
                    } } ?>
                    
                <li class="<?php if(($t_link == get_permalink()) or ($find_slug)){ ?>active<?php } ?>" ><a href="<?=$t_link?>"><?=__($item->name)?><i class="icon-<?=get_field('icon_cat_menu','services_'.$item->term_id)?>"></i></a>

                <?php    $args_posts = array(
                    'post_type' => 'service',
                    'posts_per_page' => -1,
                    //'services'    => $current_cat_ID,
                    'post_status' => 'publish',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'services',
                            'field'    => 'id',
                            'terms'    => array( $item->term_id ),

                        )
                    )
                );
                $posts_2 = new WP_Query( $args_posts );
                if($posts_2->have_posts()){ ?>
                     <ul class="main-menu-dop">
                    <?php  while ( $posts_2->have_posts() ) { $posts_2->the_post(); ?>

                        <?php
                        $icon = false;
                        $image = false;

                        $get_icon = get_field('icon');
                        if($get_icon != '0'){
                            $icon = $get_icon;
                        }

                        $image_1 = wp_get_attachment_image_url( get_field('image_1'), 'orland-banner_clients' );
                        $image_2 = wp_get_attachment_image_url( get_field('image_1_hover'), 'orland-banner_clients' );

                        if(!$image_2){ $image_2 = $image_1; }

                        $image[0] = $image_1;
                        $image[1] = $image_2; ?>

                        <?php if($icon){ ?>
                            <li><a href="<?=get_the_permalink()?>"><i class="icon-<?=$icon?>" style="font-size: 30px;"></i><span><?php the_title() ?></span></a></li>
                        <?php } elseif($image) { ?>
                            <li>
                                <a href="<?=get_the_permalink()?>">
                                    <i class="icon-dop-normal"><img src="<?=@$image[0]?>" alt=""></i>
                                    <i class="icon-dop-hover"><img src="<?=@$image[1]?>" alt=""></i>
                                    <span><?php the_title() ?></span>
                                </a>
                            </li>
                        <?php } else { ?>
                             <li><a href="<?=get_the_permalink()?>"><span><?php the_title() ?></span></a></li>
                        <?php } ?>

                    <?php } ?>
                     </ul>
                    <?php   wp_reset_postdata();  } ?>
                

                </li>

            <?php }  wp_reset_postdata(); wp_reset_query(); ?>

            <?php $menu_after_header = get_field('menu_after_header','option'); ?>
            <?php if($menu_after_header){ ?>

            <?php foreach($menu_after_header as $link){ ?>
                
                <?php $find_slug = false; if($tax_slug){
                    if(strripos($link['href'], $tax_slug) !== false){
                         $find_slug = true;
                    } } ?>


                    
            <li class="<?php if(($link['href'] == get_permalink()) or ($find_slug)){ ?>active<?php } ?>"><a href="<?=$link['href']?>"><?=__($link['title'])?><i class="icon-<?=$link['icon']?>"></i></a></li>

            
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
