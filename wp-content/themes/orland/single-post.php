<?php get_header(); ?>

<?php the_post(); ?>

<?php $category = get_the_category(get_the_ID());
    $current_cat_ID =  @$category[0]->cat_ID;
 ?>
    <div class="page-stock">
        <div class="page-title"><?=__(get_field('single_name','category_'.$current_cat_ID))?></div>
        <div class="line-27"></div>
        <div class="container-1360">
            <div class="page-stock-content">
                <img src="<?= get_the_post_thumbnail_url( get_the_ID(), 'orland-news-page' )?>" alt="">
                <span class="page-stock-date"><?php the_time('d.m.Y') ?></span>
                <span class="page-stock-name"><?php the_title(); ?></span>
                <span class="page-stock-text"><?php the_content(); ?></span>

                <?php if(($current_cat_ID) and ($current_cat_ID != 1)){ ?>

                    <div class="page-stock-btn"><a data-toggle="modal" data-target="#popups-application" href="javascript:viod(0);"><?php the_field('lng_get_message','option') ?><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="33" height="15" viewBox="0 0 33 15"><g><g transform="translate(-936 -1142)"><image width="33" height="15" transform="translate(936 1142)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAPCAYAAABqQqYpAAABuElEQVRIS63VT4iNYRTH8c9g1PjTZCGLURNbTcrGFIaMhczC3sJCYWwUEks7FlJmJoqYqSFiJv9WlAULzMJCYWFDsRBrymLQqXPr7fbe633HPXV73u49z3O+95zf73m71ItVuIHl9ba1zF6M110lP69BT9P33/AT59CHd/n8PyzbcR6/yyBOZqFigVuYw3PsxlW8xMQCKJbgDDbiMO6UQbQ6txtPMYRo4008w+UaIOtxHfcwliCjdSA24SCOZNGluIsZTFcA2Yej+e/fZP4hzNeBGMUvTBYKBsgsbmdnylhW4hJ+4Hhqq5EXY70YEBsQOmgVoYfHuIYLKcpi7jI8SH3EWozNGMfZHEFzjRfYFhAhlLVtIBrOCCFuCTWX5AbIw5xzrItwGjtwAF9K9oTNH2Fn1XGswH3sagPbmzlvMYAnaeMy6DhmK/bGFKpChKf34NQ/BBiuCYCv+WmXfgKfyyx6DP2FnVfwPjXzMZ1QwQiVUkLMMbJPzZ1Yh2hrIz6kmsOKoewg71S8wmAcVnUccVuG0jsVqzGFkToQcVMOd4ogL73oeLw7KnciqGNUfzoAEt3/jv2Ni+svznNSV6oP7mwAAAAASUVORK5CYII="/></g></g></svg></a></div>

                <?php } ?>
            </div>
        </div>
    </div>

<?php

    $args_posts = array(
        'post_type' => 'post',
        'posts_per_page' => 4,
        'cat'    => $current_cat_ID,
        'post_status' => 'publish',
        'orderby' => 'date',
        'order' => 'DESC',
        'post__not_in' => [get_the_ID()],
    );
    $posts = new WP_Query( $args_posts );
 ?>
    <?php if($posts->have_posts()){ ?>
    <div class="page-promotions">
        <div class="page-title"><?=__(get_field('more_name','category_'.$current_cat_ID))?></div>
        <div class="line-27"></div>
        <div class="container-1360">
            <div class="page-promotions-list clearfix">
                <ul>
                    <?php while ( $posts->have_posts() ) { $posts->the_post(); ?>
                        <?php get_template_part( 'templates/item-list' ); ?>
                    
                    <?php } wp_reset_postdata();  ?>

                </ul>
            </div>
        </div>
    </div>
    <?php } ?>

<?php get_footer(); ?>
