<li>
    <div class="page-promotions-list-img">
        <img src="<?= get_the_post_thumbnail_url( get_the_ID(), 'orland-news' )?>" alt="">
    </div>
    <div class="page-promotions-list-date"><?php the_time('d.m.Y') ?></div>
    <div class="page-promotions-list-name"><?php the_title(); ?></div>
    <div class="page-promotions-list-text"><?= kama_excerpt(); ?></div>
    <div class="page-promotions-list-btn"><a href="<?php the_permalink() ?>"><?php the_field('lng_more','option') ?></a></div>
</li>
