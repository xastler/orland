<?php /* Template Name: Vacancies */ ?>
<?php get_header(); ?>

    <div class="page-vacancies">
        <div class="page-title"><?php the_title() ?></div>
        <div class="line-27"></div>
        <?php $block_vac = get_field('block_vac') ?>
        <?php if($block_vac){ ?>
        <div class="page-vacancies-info">
            <div class="container-content">
                <a href="<?=__(@$block_vac[0]['link_more'])?>" class="page-vacancies-info-baner">
                    <p><?=__(@$block_vac[0]['title_1'])?></p>
                    <span><?php the_field('lng_more','option') ?>>></span>
                </a>
                <div class="page-vacancies-info-text">
                    <div class="page-vacancies-info-title"><?=__(@$block_vac[0]['info_title'])?></div>
                    <p><?=__(@$block_vac[0]['info_title_descr'])?></p>

                </div>
            </div>
        </div>
        <?php } ?>
        <div class="page-vacancies-form">
            
            <h2><?php the_field('anket_title') ?></h2>
            <i class="icon-new-file"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="28" height="22" viewBox="0 0 28 22"><g><g transform="translate(-807 -1185)"><image width="28" height="22" transform="translate(807 1185)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAWCAYAAADTlvzyAAABdElEQVRIS+3WvUoDQRAH8P/smUIRFCxS2M29gOADeKktBNEnsLMUxcLGRvABfADzCqKNkGAjWIhgl2I/zusVQSGN2ZENCQQxH3J7nddctz/+czN7Q8zcArCCCh8iujHGnASCmFlEpElEeRUmEWXhXGNM/90HlVINrfVdFSAznwb0Hxxb3TRN12u1Wt7pdF5n+QSlSppl2VxRFF0ACYBnImr1er2LPM/HNlwpMCRi5gcAnwAeAWwDWBg0hf4tcWkwTdMzEdm01q4x8xKAWwCr49AYYENE2iKy5Zy7moaWBgdlPQdwICI709Ao4F/QaOCsaABFZMM514hytTHzxPImSdL13te11k9RwFmTDkcm2uU9Lakxpj+n0cAJSdsA6sM5jQr+RInIAQjgPID3gEYHR1EAHwDulVJ73vuwWSxXAo6gxyKyq5R6E5FrAJdVrBg+rCzOuZdhIwH4AtC01u4HMPYSJQAOrbXh3PB3CSvGojHmCIB8A4YaYvjIWK8fAAAAAElFTkSuQmCC"/></g></g></svg></i>
            <p class="form-text"><?php the_field('anket_descr') ?></p>

            <?php  the_content() ?>
            <?php if(0){ ?>
                        <form>
                <div class="page-vacancies-form-row">
                    <div class="page-vacancies-form-name">Ф.И.О.</div>
                    <input type="text" name="fio" placeholder="Ф.И.О." required>
                </div>
                <div class="page-vacancies-form-row">
                    <div class="page-vacancies-form-name">Дата рождения</div>
                    <input type="text" name="date" placeholder="" required>
                </div>
                <div class="page-vacancies-form-row">
                    <div class="page-vacancies-form-name">Адрес</div>
                    <div class="page-vacancies-form-inputs">
                        <input type="text" name="street" placeholder="Улица, дом" required>
                        <input type="text" name="city" placeholder="Город" required>
                        <input type="text" name="city-id" placeholder="Индекс" required>
                    </div>
                </div>
                <div class="page-vacancies-form-row">
                    <div class="page-vacancies-form-name">Паспортные данные</div>
                    <input type="text" name="passport" placeholder="(серия, номер, когда и кем выдан)" required>
                </div>
                <div class="page-vacancies-form-row">
                    <div class="page-vacancies-form-name">В настоящее время вы <br> являетесь:</div>
                    <div class="page-vacancies-form-select">
                        <select class="select">
                            <option selected value="трудоустроенным">трудоустроенным</option>
                            <option value="безработный">безработный</option>
                            <option value="на пол ставки">на пол ставки</option>
                        </select>
                    </div>
                </div>
                <div class="page-vacancies-form-row">
                    <div class="page-vacancies-form-name">Предпочитаемый <br>график работы:</div>
                    <div class="page-vacancies-form-select">
                        <select class="select">
                            <option selected value="1/2">1/2</option>
                            <option value="1/1">1/1</option>
                            <option value="1/4">1/4</option>
                            <option value="5/2">5/2</option>
                        </select>
                    </div>
                </div>
                <div class="page-vacancies-form-row">
                    <div class="page-vacancies-form-name">Последнее место <br> работы</div>
                    <input type="text" name="last-work" placeholder="" required>
                </div>
                <div class="page-vacancies-form-row">
                    <div class="page-vacancies-form-name">Дополнительные <br>сведения:</div>
                    <input type="text" name="additional-information" placeholder="" required>
                </div>
                <div class="page-vacancies-form-row">
                    <div class="page-vacancies-form-name">Обратная связь <br>(E-mail, телефон)</div>
                    <input type="text" name="call-back" placeholder="" required>
                </div>
                <p>Отправляя личные данные через контактную форму Вы автоматически соглашаетесь на проверку информации
                    <br>указанной в анкете.</p>
                <div class="page-vacancies-form-btn"><button>Отправить</button></div>
            </form>
            <?php } ?>
        </div>

    </div>
<?php get_footer(); ?>

