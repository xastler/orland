<?php /* Template Name: Home */ ?>
<?php get_header(); ?>

    <div class="find-cost _container_cost">
        <div class=" _add_mask_cl"></div>
        <div action="">
            <div class="container-content" style="position: relative;">

                <div class="find-cost-form">

                <?php if(qtranxf_getLanguage() == 'en'){ ?>
                    <?php _e(do_shortcode('[contact-form-7 id="451" title="form_find_cost_home_en"]')); ?>
                <?php } else { ?>
                    <?php _e(do_shortcode('[contact-form-7 id="449" title="form_find_cost_home"]')); ?>
                <?php } ?>
                    
                    

                    <style>
                        ._service_service_change , ._service_object_change , ._service_options_change{
                            display : none;
                        }
                        .find-cost-form .find-cost-form-inputs p:last-child{
                            display : none;
                        }
                        ._mask{
                            position: absolute;
                            width: 100%;
                            height: 100%;
                            background: rgba(255,255,255,0.5);
                            z-index: 9;
                        }
                    </style>
                    
                    <?php if(0){ ?>
                    <form action=""> 
                        <h2>Заголовок</h2>
                        <div class="find-cost-form-inputs">
                            <input type="text" name="name" placeholder="Имя" required>
                            <input type="text" name="phone" placeholder="Телефон" required>
                            <input type="email" name="email" placeholder="E-mail" required>
                        </div>
                        <button type="submit">Отправить</button>
                    </form>
                    <?php } ?>
                </div>
                <div class="find-cost-main">
                    <div class="line-27"></div>
                    <p><?php the_field('lng_get_service_title','option') ?></p>
                    <div class="question-1"></div>
                    <div class="question-2"></div>
                </div>

                <?php $args = array(
                        'taxonomy' => 'services',
                        'hide_empty' => false,
                    );
                    $terms = get_terms( $args ); ?>
                
                <div class="find-cost-lvl form-start form-lvl-1">
                    <div class="find-cost-text">
                        <span>1</span>
                        <p><?php the_field('lng_get_service_','option') ?></p>
                    </div>
                    <select name="service" class="select _service_service_change">
                        <?php $first = true; ?>
                        <?php foreach($terms as $item){ ?>
                            <option value="<?=$item->term_id?>" <?php if($first){ $first = false; ?>selected<?php } ?>><?=__($item->name)?></option>
                        <?php } ?>
                    </select>
                </div>
                
                <div class="find-cost-lvl form-lvl-2">
                    <div class="find-cost-text">
                        <span>2</span>
                        <p><?php the_field('lng_get_ob','option') ?></p>
                    </div>
                    <select name="lvl-2" class="select _service_object_change">
                        <option value="t1" selected><?php echo str_replace(['<br>','<br/>','<br />'],'',get_field('lng_get_ob','option')) ?></option>
                        <option value="t2">1</option>

                    </select>
                </div>
                <div class="find-cost-lvl form-lvl-3">
                    <div class="find-cost-text">
                        <span>3</span>
                        <p><?php echo str_replace(['<br>','<br/>','<br />'],'',get_field('lng_get_par','option')) ?></p>
                    </div>
                    <select name="lvl-3" class="select _service_options_change">
                        <option value="t1" selected><?php echo str_replace(['<br>','<br/>','<br />'],'',get_field('lng_get_par','option')) ?></option>
                        <option value="t2">1</option>

                    </select>
                </div>
        </div>
        </div>
    </div>
    
    <div class="trust-us">
        <div class='page-title'><?php the_field('lng_trust_us','option') ?> </div>
        <div class="line-27"></div>
        <div class="container-content">
            <?php $block_trust_us = get_field('block_trust_us'); ?>
            <?php if($block_trust_us){ ?>
            <?php foreach($block_trust_us as $block_trust){ ?>
            
            <div class="trust-us-block">
                <img src="<?= wp_get_attachment_image_url( @$block_trust['image'], 'orland-block_trust_us' )?>" alt="">
                <p><?=__($block_trust['title'])?></p>
                <a href="<?=$block_trust['href']?>"><?php the_field('get_more','option') ?></a>
            </div>
            <?php } ?>
            <?php } ?>
            
        </div>
    </div>
    <div class="home-baner_info parallax-window">
        <div class="parallax"><img src="<?=get_template_directory_uri()?>/orland/build/img/home-baner_info.png" alt=""></div>
        <h1><?php the_field('lng_security_company','option') ?> </h1>
        <div class="line-27"></div>
        <div class="container-full">
            
            <?php $block_security_company = get_field('block_security_company'); ?>
            <?php if($block_security_company){ ?>
            <?php foreach($block_security_company as $block_company){ ?>
            <div class="home-baner_info-block">
                <img src="<?= wp_get_attachment_image_url( @$block_company['image'], 'orland-block_trust_us' )?>" alt="">
                <div class="home-baner_info-num"><span><?=__(@$block_company['qty'])?></span>+</div>
                <div class="home-baner_info-name"><?=__(@$block_company['title'])?></div>
            </div>
            <?php } ?>
            <?php } ?>

        </div>
    </div>
    <div class="home-tabs">
        <div class='page-title'><?php the_field('lng_trust_we','option') ?> </div>
        <div class="line-27"></div>
        <div class="container-full">
            <div id="home-tabs">
                <?php $block_we_trust = get_field('block_we_trust'); ?>
                
                <ul class="home-tabs-list">
                    <?php if($block_we_trust){ ?>
                    <?php foreach($block_we_trust as $key => $block_we_t){ ?>
                    <li><a href="#tabs-<?=($key+1)?>"><?=__($block_we_t['category'])?></a></li>
                    <?php } ?>
                    <?php } ?>
                </ul>

                <?php if($block_we_trust){ ?>
                <?php foreach($block_we_trust as $key => $block_we_t){ ?>
                <div id="tabs-<?=($key+1)?>" class="home-tab">
                    <div class="home-tab-slicks">
                        <?php foreach($block_we_t['elements'] as $img){ ?>
                        <div class="home-tab-slick">
                            <img src="<?= wp_get_attachment_image_url( @$img['image'], 'orland-block_we_trust' )?>" alt="">
                        </div>
                        <?php } ?>
                    </div>
                </div>
                
                <?php } ?>
                <?php } ?>
                
            </div>
        </div>
    </div>

    <?php $args_reviews = array(
            'post_type' => 'reviews',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'orderby' => 'date',
            'order' => 'DESC',
        );
    $reviews = new WP_Query( $args_reviews );
    ?>
    <?php if($reviews->have_posts()){ ?>
    <div class="home-reviews">
        <div class='page-title'><?php the_field('lng_more_about_company','option') ?></div>
        <div class="line-27"></div>
        <div class="container-full">
            <div class="home-reviews-sliser">
                <?php while ( $reviews->have_posts() ) { $reviews->the_post(); ?>
                <div class="home-review">
                    <div class="home-review-name"><?php the_title(); ?> </div>
                    <div class="home-review-position"><?=__(get_field('name2'))?></div>
                    <div class="line-20"></div>
                    <div class="home-review-info"><?php _e(get_the_excerpt(get_the_ID())); ?></div>
                </div>
                <?php } ?>

            </div>

        </div>
    </div>
    
    <?php } wp_reset_postdata(); ?>
    
    <?php  $home_get_cat_id = get_field('home_get_cat_id'); ?>
    
    <?php if($home_get_cat_id){ ?>
        
    <?php $args_posts = array(
            'post_type' => 'post',
            'posts_per_page' => 4,
            'cat'    => $home_get_cat_id,
            'post_status' => 'publish',
            'orderby' => 'date',
            'order' => 'DESC',
        );
    $posts = new WP_Query( $args_posts );
 ?>
 <?php if($posts->have_posts()){ ?>
    <div class="home-news">
        <div class='page-title'><?= __(get_cat_name($home_get_cat_id)); ?></div>
        <div class="line-27"></div>
        <div class="container-full">
            <?php while ( $posts->have_posts() ) { $posts->the_post(); ?>
            
            <div class="home-news-block">
                <div class="home-news-img">
                    <img src="<?= get_the_post_thumbnail_url( get_the_ID(), 'orland-news' )?>" alt="news">
                </div>
                <div class="home-news-content">
                    <div class="home-news-date"><?php the_time('d.m.Y') ?></div>
                    <div class="home-news-name"><?php the_title(); ?></div>
                    <div class="home-news-text"><?php the_excerpt(); ?></div>
                    <div class="home-news-next"><a href="<?php the_permalink() ?>"><?php the_field('lng_more','option') ?> >></a></div>
                </div>
            </div>

            <?php }   ?>

        </div>
        <div class="home-news-open"><a href="<?=get_category_link($home_get_cat_id)?>"><?=__(get_field('more_name_2','category_'.$home_get_cat_id))?></a></div>
    </div>

    <?php } wp_reset_postdata(); ?>
    <?php } ?>

<?php get_footer(); ?>

