<?php /* Template Name: Contacts */ ?>
<?php get_header(); ?>

    <div class="page-promotions">
        <div class="page-title"><?php the_title() ?></div>
        <div class="line-27"></div>
        <div class="container-1360">
            <div class="page-promotions-content">
                <div class="page-promotions-city">
                    <?php the_field('content_'); ?>
                    <div class="page-promotions-info">

                        <?php $regions = get_field('regions'); ?>
                        
                        <?php if($regions){ ?>
                        <?php $key = 1; ?>
                        <?php foreach($regions as $region){ ?>

                        <div class="page-promotions-city-block map-regions <?php if($key == 1){ ?>active<?php } ?>">
                            <div class="page-promotions-city-name " ><?=__($region['name_region'])?></div>

                            <div class="page-promotions-city-info" <?php if($key == 1){ ?>style="display: block;"<?php } ?>>
                                <?php if($region['contacts']){ ?>
                                <?php foreach($region['contacts'] as $contact){ ?>
                                
                                <div class="block-top">
                                    <div class="page-promotions-city-address " data-point-x="<?=$contact['map-x']?>" data-point-y="<?=$contact['map-y']?>" data-gps-img="<?=get_template_directory_uri()?>/orland/build/img/gps.png" data-zoom="<?=$contact['map-zoom']?>">
                                        <i class="icon-flag"></i>
                                        <?=__($contact['city_name'])?> <br>
                                        <?=__($contact['info'])?>
                                    </div>
                                    <div class="page-promotions-city-email">
                                        <i class="icon-letter"></i>
                                        <a href="mail:<?=__($contact['email'])?>"><?=__($contact['email'])?></a>
                                    </div>
                                </div>
                                <div class="page-promotions-city-phone">
                                    <i class="icon-phone-reciever"></i>
                                    <?php if($contact['phones']){ ?>
                                    <?php foreach($contact['phones'] as $phone){ ?>
                                <p><a href="tel::<?= str_replace(['-',' ',')','('],'',__($phone['phone'])); ?>"><?=__($phone['phone'])?></a></p>
                                    <?php } ?>
                                    <?php } ?>
                                </div>

                                <?php } ?>
                                <?php } ?>

                            </div>

                        </div>
                        
                        <?php $key++; } ?>
                        <?php } ?>
                        
                    </div>
                </div>
                <div class="page-promotions-map" id="map"></div>
            </div>
        </div>
    </div>
    <div class="container-1360">
        <div class="questions-remain">
            <div class="page-title"><?php the_field('content_get_q'); ?></div>
            <p><?php the_field('content_get_q2'); ?></p>
            <div class="line-27"></div>

            <?php the_content(); ?>

            <?php if(0){ ?>
            <form>
                <div class="questions-remain-field">
                    <div class="questions-remain-inputs">
                        <input type="text" name="name" placeholder="Имя" required>
                        <input type="text" name="email" placeholder="E-mail" required>
                    </div>
                    <div class="questions-remain-textarea">
                        <textarea name="text" id="" placeholder="Сообщение"></textarea>
                    </div>
                </div>
                <div class="questions-remain-btn ">
                    <button type="submit">Отправить</button>
                </div>
            </form>
        <?php } ?>
            
        </div>
    </div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbBJylV7czEp-mYp44dPWd7JmoD2Lk3rQ" type="text/javascript"></script>
<?php get_footer(); ?>

