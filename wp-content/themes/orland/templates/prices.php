<?php /* Template Name: Prices */ ?>
<?php get_header(); ?>


<?php $args = array(
    'taxonomy' => 'services',
    'hide_empty' => false,
    'orderby'       => 'id', 
    'order'         => 'ASC',
);
$terms = get_terms( $args );

//var_dump($terms);

?>

    
    <div class="prise-list">
        <div class="page-title"><?php the_title(); ?></div>
        <div class="line-27"></div>
        <div id="prise-list-tabs" class="left-tabs">
            <ul class="left-tabs-list">
                <?php foreach($terms as $key => $item){ ?>
                <li><a href="#left-tabs-<?=$key?>"><?=__($item->name)?></a><i class="icon-<?=get_field('icon_cat_menu','services_'.$item->term_id)?>"></i></li>
                <?php } ?>
            </ul>
            <div class="left-tabs-content prise-list-content">
                <?php foreach($terms as $key => $item){ ?>
                <div id="left-tabs-<?=$key?>" class="left-tab _tab_cntn">
                    <div class="prise-list-title"><?= str_replace(['<','br','>','/'],'',get_field('lng_get_service_','option')); ?></div>
                    <div class="prise-list-select">
                        
                    <?php   $args_posts = array(
                        'post_type' => 'service',
                        'posts_per_page' => -1,
                        //'services'    => $current_cat_ID,
                        'post_status' => 'publish',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'services',
                                'field'    => 'id',
                                'terms'    => array( $item->term_id ),

                            )
                        )
                    );
                    $posts_2 = new WP_Query( $args_posts ); ?>

                    <?php $first_elem = false; ?>

                        <select name="get_service" class="select _get_service">
                            <?php if($posts_2->have_posts()){ ?>
                                <?php  while ( $posts_2->have_posts() ) { $posts_2->the_post(); ?>
                                <option value="<?=get_the_ID();?>" <?php if(!$first_elem){ $first_elem = get_the_ID(); ?>selected<?php } ?>><?php the_title() ?></option>
                            <?php } ?>
                            <?php }  wp_reset_postdata(); ?>
                        </select>
                    </div>

                    <?php $prices = get_field('_prices',$first_elem); ?>

                    <div class="_price_list_content">
                        
                    <?php foreach($prices as $item){ ?>
                    
                    <div class="prise-list-table">
                        <div class="table-name"><?=__($item['name'])?></div>
                        <?=__($item['table'])?>

                    </div>
                    <div class="prise-list-btn">
                        <a data-toggle="modal" href="#popups-application-get-price"><?php the_field('lng_get_get','option') ?></a>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21" height="21" viewBox="0 0 21 21"><g><g transform="translate(-894 -1448)"><image width="21" height="21" transform="translate(894 1448)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAA8klEQVQ4T+3UPUoDQRQA4C/YewQjGIKkygm0sxIhrfdIb+0BPEAKaxFsLbyA6AkExcpCPYE82JE47k9m3dLX7TLz7Zt57+3IsHGC5WhAM8BLnA6FfoO4DvQOO4UZ32NR7fkBxrtA3yNlvBbAb3jBL3AdneOpAI2lteBf0BzcxmdKKh2/JNMc3I/iYNoXrTtyJHSF3T5o0x22ons4wzE+sqI1FgWt6DNWmOBoDW4D49udx9/K4MM0elUx6rquE41NCY7F42oworpNsRGa4HPc4qZjKDZGS4brH62fqGj4+JX1iRku8jF9qFqnD5j2POIgPXwBvgRUJ6JXPgEAAAAASUVORK5CYII="/></g></g></svg>
                    </div>
                <?php } ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        </div>
<div class="action-baners">
    <div class="container-full">
        <?php $banner_s = get_field('banner_s') ?>
        <?php if($banner_s){ ?>
            <?php foreach($banner_s as $banner){ ?>

                <div class="action-baner">
                    <a href="<?=__($banner['url'])?>">
                        <img src="<?= wp_get_attachment_image_url( @$banner['image'], 'orland-banner_clients' )?>" alt="">
                        <div class="action-baner-content">
                            <div class="action-baner-title"><?=__($banner['title_1'])?></div>
                            <div class="action-baner-name"><?=__($banner['title_2'])?></div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        <?php } ?>

    </div>
</div>
    </div>




    
<?php get_footer(); ?>

