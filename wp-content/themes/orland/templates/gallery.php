<?php /* Template Name: Gallery */ ?>
<?php get_header(); ?>
    <div class="page-galery">
        <div class="page-title"><?php the_title(); ?></div>
        <div class="line-27"></div>

        <?php $gallery = get_field('gallery'); ?>
        <?php if($gallery){ ?>
        
        <div id="page-galery-tabs" class="left-tabs">
            <ul class="left-tabs-list">
                <?php $i_item = 1; ?>
                <?php foreach($gallery as $item){ ?>
                <li><a href="#left-tabs-<?=$i_item?>"><?=__(@$item['title'])?></a><i class="icon-<?=@$item['icon']?> "></i></li>
                <?php $i_item++; } ?>
            </ul>
            <div class="left-tabs-content page-galery-content">
                <?php $i_item = 1; ?>
                <?php foreach($gallery as $item){ ?>
                    
                <div id="left-tabs-<?=$i_item?>" class="left-tab">
                    <ul class="page-galery-list">
                        <?php if($item['images']){ ?>
                        <?php foreach($item['images'] as $image){ ?>

                        <?php if(!empty($image['video'])){ ?>
                            
                        <li data-img="" data-video="<?=$image['video']?>" data-toggle="modal" data-target="#popups-galery">
                            <img src="<?= wp_get_attachment_image_url( @$image['image'], 'orland-gallery' )?>" alt="">
                        </li>
                        
                        <?php } else { ?>
                            
                        <li data-img="<?= wp_get_attachment_image_url( @$image['image'], 'full' )?>" data-video="" data-toggle="modal" data-target="#popups-galery">
                            <img src="<?= wp_get_attachment_image_url( @$image['image'], 'orland-gallery' )?>" alt="">
                        </li>
                        
                        <?php } ?>

                        <?php } ?>
                        <?php } ?>
                        
                    </ul>
                </div>
                <?php $i_item++; } ?>

            </div>
        </div>
        <?php } else { ?>
            Empty
        <?php } ?>
    </div>

</div>
<?php get_footer(); ?>

