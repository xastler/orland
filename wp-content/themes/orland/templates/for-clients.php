<?php /* Template Name: For clients */ ?>
<?php get_header(); ?>
    <div class="for-clients">
        <div class="page-title"><?php the_title() ?></div>
        <div class="line-27"></div>
        <?php $clients_tabs = get_field('clients_tabs'); ?>
        <?php if($clients_tabs){ ?>
        <div id="for-clients-tabs" class="left-tabs">
            <ul class="left-tabs-list">
                
                <?php $key = 1; ?>
                <?php foreach($clients_tabs as $tab){ ?>
                <li><a data-click="#<?=$key?>" href="#left-tabs-<?=$key?>"><?=__(@$tab['title'])?></a><i class="icon-<?=$tab['icon']?>"></i></li>
                <?php $key++; } ?>
                
            </ul>
            <div class="left-tabs-content for-clients-content">
                
            <?php $key = 1; ?>
            <?php foreach($clients_tabs as $tab){ ?>
                <div id="left-tabs-<?=$key?>" class="left-tab">
                    <div class="for-clients-title_tab"><?=__(@$tab['title'])?></div>
                    
                    <?php if($tab['items']){ ?>
                    <?php foreach($tab['items'] as $item){ ?>
                        
                    <div class="for-clients-list">
                        <div class="for-clients-list-name"><?=__(@$item['name'])?></div>
                        <ul>
                            
                        <?php if($item['lists']){ ?>
                        <?php foreach($item['lists'] as $element){ ?>
                            <?php if(!$element['doc']){ ?>
                                <li class="fc-list-text">- <?=__(@$element['text'])?></li>
                            <?php } else { ?>
                                <li  class="fc-list-document"><a download href="<?=$element['doc']?>">- <?=__(@$element['text'])?></a></li>
                            <?php } ?>
                        <?php } ?>
                        <?php } ?>

                        </ul>
                    </div>
                    <?php } ?>
                    <?php } ?>

                </div>
                <?php $key++; } ?>

            </div>
        </div>
        <?php } ?>
    </div>
<div class="action-baners">
    <div class="container-full">
        <?php $banner_s = get_field('banner_s') ?>
        <?php if($banner_s){ ?>
            <?php foreach($banner_s as $banner){ ?>

                <div class="action-baner">
                    <a href="<?=__($banner['url'])?>">
                        <img src="<?= wp_get_attachment_image_url( @$banner['image'], 'orland-banner_clients' )?>" alt="">
                        <div class="action-baner-content">
                            <div class="action-baner-title"><?=__($banner['title_1'])?></div>
                            <div class="action-baner-name"><?=__($banner['title_2'])?></div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        <?php } ?>

    </div>
</div>
</div>


    
<?php get_footer(); ?>

