<?php get_header(); ?>
    
    <div class="page-promotions">
        <div class="page-title"><?php single_term_title(); ?></div>
        <div class="line-27"></div>
        <div class="container-1360">
            <?php if( have_posts() ){ ?>
            <div class="page-promotions-list clearfix">
                <ul>
                    <?php while ( have_posts() ){ the_post(); ?>

                        <?php get_template_part( 'templates/item-list' ); ?>

                    <?php } ?>

                </ul>
            </div>

            <?php  wpbeginner_numeric_posts_nav(); ?>

        <?php } ?>
            
        </div>
        
    </div>

<?php get_footer(); ?>
