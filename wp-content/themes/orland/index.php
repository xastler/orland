<?php get_header(); ?>

<?php the_post(); ?>
    <section class="section type_page first_ssection">
        <div class="container">
            <div class="text-center">
                <div class="type_title"><?php the_title() ?></div>
            </div>
            <div class="types_container text-about">
                <?php the_content() ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>

