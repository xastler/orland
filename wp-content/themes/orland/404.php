<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="page-stock">
    <div class="page-title"><h1>404<h1></div>
    <div class="line-27"></div>
    <div class="container-1360">
        <div class="page-stock-content">
            
            <div class="page-stock-text"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentyseventeen' ); ?></div>

        </div>
    </div>
</div>


<?php
get_footer();
