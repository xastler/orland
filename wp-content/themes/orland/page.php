<?php get_header(); ?>

<?php the_post(); ?>

<div class="page-stock">
    <div class="page-title"><h1><?php the_title(); ?><h1></div>
    <div class="line-27"></div>
    <div class="container-1360">
        <div class="page-stock-content">
            
            <div class="page-stock-text text-about"><?php the_content(); ?></div>

        </div>
    </div>
</div>


<?php get_footer(); ?>
