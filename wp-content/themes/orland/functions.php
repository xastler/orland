<?php

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
    acf_add_options_page(['page_title' => 'Translations', 'menu_slug' => 'translate-options' ]);
}

//add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
      show_admin_bar(false);
    }

    if ( is_admin() ) {
        if((!current_user_can('administrator')) && (!isset($_POST['action']))){
            wp_safe_redirect( home_url() );
        }
    }
}

add_filter('style_loader_tag', 'sj_remove_type_attr', 100, 2);
add_filter('script_loader_tag', 'sj_remove_type_attr', 100, 2);
add_filter('wp_print_footer_scripts ', 'sj_remove_type_attr', 100, 2);
function sj_remove_type_attr($tag) {
    return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}


function orland_setup() {

    load_theme_textdomain( 'orland' );

    add_theme_support( 'title-tag' );

    add_theme_support( 'post-thumbnails' );

    add_image_size( 'orland-home-slider', 1595, 678, true );
    
    add_image_size( 'orland-block_trust_us', 103, 91 );
    add_image_size( 'orland-block_we_trust', 212, 268 );
    add_image_size( 'orland-gallery', 245, 245 ,true);
    add_image_size( 'orland-banner_clients', 709, 252 );
    
    add_image_size( 'orland-news', 304, 232 );
    add_image_size( 'orland-news-page', 538, 398 );
    add_image_size( 'orland-footer-logo', 381, 497 );
    
    add_image_size( 'orland-baner_page', 1595, 486,true );
    add_image_size( 'full_shuviha', 620, 591, true );
    
    add_image_size( 'orland-single_service_img_pos', 696, 525,true );

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus(
        array(
            'top'    => __( 'Главное меню', 'orland' ),
            'left'    => __( 'Левое меню', 'orland' ),
            'footer_service' => __( 'Footer Услуги', 'orland' ),
            'footer_company' => __( 'Footer О компании', 'orland' ),
            'footer_klients' => __( 'Footer Клиентам', 'orland' ),
        )
    );

    add_theme_support(
        'custom-logo', array(
            'width'      => 157,
            'height'     => 206,
            'flex-width' => true,
        )
    );

}
add_action( 'after_setup_theme', 'orland_setup' );

add_filter( 'excerpt_length', function(){
    return 19;
} );
add_filter('excerpt_more', function($more) {
	return '...';
});


function get_logo_site_src() {
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $image_attributes = wp_get_attachment_image_src( $custom_logo_id ,array(157,206));
    return isset($image_attributes[0]) ? $image_attributes[0] : get_template_directory_uri()."/orland/build/img/header-logo.png";
}

function orland_scripts() {

    wp_enqueue_style( 'orland-fonts', "https://use.fontawesome.com/releases/v5.0.10/css/all.css");

    wp_enqueue_style( 'orland-libs',  get_theme_file_uri( '/orland/build/css/libs.css' ), array( 'orland-fonts' ) );

    wp_enqueue_style( 'orland-style',  get_theme_file_uri( '/orland/build/css/style.css' ), array( 'orland-libs' ) );
    
    wp_enqueue_style( 'orland-fonts-2',  "https://fonts.googleapis.com/css?family=Montserrat", array( 'orland-style' ) );

    
    //wp_enqueue_style( 'twentyseventeen-style', get_stylesheet_uri() );

    
    wp_enqueue_script( 'orland-libs', get_theme_file_uri( '/orland/build/js/libs.js' ), array(), '1.0', 1 );

    wp_enqueue_script( 'orland-app', get_theme_file_uri( '/orland/build/js/app.js' ), array('orland-libs'), '1.0', 1 );

    
    wp_enqueue_script( 'orland-back-js', get_theme_file_uri( '/back_js.js' ), array('orland-app'), '1.0', true );

}

add_action( 'wp_enqueue_scripts', 'orland_scripts' );



function change_post_menu_label() {
        global $menu, $submenu;
        $menu[5][0] = 'Новости/Акции';
        $submenu['edit.php'][5][0] = 'Новости/Акции';
        $submenu['edit.php'][10][0] = 'Добавить Новость';
        $submenu['edit.php'][16][0] = 'Новости метки';
        echo '';
    }
    
add_action( 'admin_menu', 'change_post_menu_label' );
    
function change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Новости';
    $labels->singular_name = 'Новости';
    $labels->add_new = 'Добавить Новость';
    $labels->add_new_item = 'Добавить Новость';
    $labels->edit_item = 'Редактировать Новость';
    $labels->new_item = 'Добавить Новость';
    $labels->view_item = 'Посмотреть Новость';
    $labels->search_items = 'Найти Новость';
    $labels->not_found = 'Не найдено';
    $labels->not_found_in_trash = 'Корзина пуста';
}

add_action( 'init', 'change_post_object_label' );



function register_post_types(){
    register_post_type('reviews', array(
        'label'  => 'Отзывы о компании',
        'labels' => array(
            'name'               => 'Отзывы о компании', // основное название для типа записи
            'singular_name'      => 'Отзывы о компании', // название для одной записи этого типа
            'add_new'            => 'Добавить Отзыв', // для добавления новой записи
            'add_new_item'       => 'Добавление Отзыв', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование Отзыв', // для редактирования типа записи
            'new_item'           => 'Новое Отзыв', // текст новой записи
            'view_item'          => 'Смотреть Отзыв', // для просмотра записи этого типа.
            'search_items'       => 'Искать Отзыв', // для поиска по этим типам записи
            'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Отзывы о компании', // название меню
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => false, // зависит от public
		'exclude_from_search' => false, // зависит от public
		'show_ui'             => true,
        'show_in_menu'        => true, // показывать ли в меню адмнки
        'hierarchical'        => false,
        'supports'            => array('title','excerpt'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'has_archive'         => false,

    ) );

    register_post_type('service', array(
        'label'  => 'Услуги',
        'labels' => array(
            'name'               => 'Услуги', // основное название для типа записи
            'singular_name'      => 'Услуги', // название для одной записи этого типа
            'add_new'            => 'Добавить Услугу', // для добавления новой записи
            'add_new_item'       => 'Добавление Услуги', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование Услуги', // для редактирования типа записи
            'new_item'           => 'Новая Услуга', // текст новой записи
            'view_item'          => 'Смотреть Услугу', // для просмотра записи этого типа.
            'search_items'       => 'Искать Услугу', // для поиска по этим типам записи
            'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Услуги', // название меню
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => true, // зависит от public
		'exclude_from_search' => false, // зависит от public
		'show_ui'             => true,
        'show_in_menu'        => true, // показывать ли в меню адмнки
        'hierarchical'        => false,
        'taxonomies'          => ['services'],
        'rewrite'             => 'service',
        'supports'            => array('title','excerpt','editor','thumbnail'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'has_archive'         => false,

    ) );

    register_taxonomy('services', array('service'), array(
		'label'                 => 'Категории', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Категории',
			'singular_name'     => 'Категории',
			'search_items'      => 'Search Категории',
			'all_items'         => 'All Категории',
			'view_item '        => 'View Категории',
			'parent_item'       => 'Parent Категории',
			'parent_item_colon' => 'Parent Категории:',
			'edit_item'         => 'Edit Категории',
			'update_item'       => 'Update Категории',
			'add_new_item'      => 'Add New Категории',
			'new_item_name'     => 'New Категории Name',
			'menu_name'         => 'Категории',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'hierarchical'          => true,
		'update_count_callback' => '',
		'rewrite'               => 'services',
		//'query_var'             => $taxonomy, // название параметра запроса
        ));

}

add_action('init', 'register_post_types');

function true_load_posts(){

    $method = isset($_POST['method']) ? stripslashes( $_POST['method'] ) : false;
    $page = (isset($_POST['page']) ? stripslashes( $_POST['page'] ) : 0)+1;

    $return_data = [];
    $return_data['hide_button'] = true;
    $return_data['page'] = $page;
    $return_data['data'] = '';

    if($method == 'latest'){
        $args = array(
            'paged' => (int)$page,
            'post_type' => 'post',
            'post_status' => 'publish',
            'orderby' => 'date',
            'order' => 'DESC',
        );
    } elseif($method == 'popular'){
        $args = array(
            'paged' => (int)$page,
            'post_type' => 'post',
            'post_status' => 'publish',
            'meta_key' => '_like_summ',
            'orderby' => 'meta_value_num',
            'order'	 => 'DESC'
        );
    } else {
        exit();
    }
        query_posts( $args );
        global $wp_query;
        $return_data['max_pages'] = $wp_query->max_num_pages;

        if($wp_query->max_num_pages > $page){
            $return_data['hide_button'] = false;
        }

        if( have_posts() ) {
            
            ob_start();
            while( have_posts() ){
                the_post();
                get_template_part( 'template-parts/participants_item' );
            }
            $return_data['data'] = ob_get_clean();
        }
    
    wp_send_json( $return_data );
    exit();
}
 
 
add_action('wp_ajax_loadmore_history', 'true_load_posts');
add_action('wp_ajax_nopriv_loadmore_history', 'true_load_posts');

function history_add_form(){

    $return = [];
    $return['status'] = false;

    $avatar = isset($_POST['avatar']) ? (int)$_POST['avatar']  : false;
    $email = isset($_POST['email']) ? stripslashes( $_POST['email'] ) : false;
    $content = isset($_POST['content']) ? stripslashes( $_POST['content'] ) : false;

    if(!$avatar){
        $return['error']['avatar'] = true;
    }
    if (!is_email($email)) {
        $return['error']['email'] = true;
    }
    if ((mb_strlen($content) < 3) || (mb_strlen($content) > 280)) {
        $return['error']['content'] = true;
    }

    $current_user = wp_get_current_user();
    
    if(!isset($return['error']) and ($current_user != 0)){
        $post_data = array(
            'post_title'    => wp_strip_all_tags( $current_user->display_name ),
            'post_content'  => $content,
            'post_status'   => 'pending',
            'post_author'   => $current_user->ID,

        );

        if(!count_user_posts_by_type($current_user->ID)){
            updates_user_like_to_relike($current_user->ID);
        }

        $post_id = wp_insert_post( $post_data );
        
        update_field('avatar', (int)$avatar, $post_id);
        update_field('like', 0, $post_id);
        update_field('relike', 0, $post_id);
        update_field('email', $email, $post_id);
        update_post_meta( $post_id, '_like_summ', 0 );

        update_user_meta( $current_user->ID, '_email_to_history', $email );


        
        if($post_id){
            $return['status'] = true;
        }
    
    }

    wp_send_json( $return );
    exit();
}

add_action('wp_ajax_history_add_form', 'history_add_form');
add_action('wp_ajax_nopriv_history_add_form', 'history_add_form');

function get_object_form(){
    $return = [];
    $return['status'] = false;

    $term_id = isset($_POST['term_id']) ? (int)$_POST['term_id']  : false;
    $return['term_id'] = $term_id;
    $return['objects'] = [];

    $args_posts = array(
        'post_type' => 'service',
        'posts_per_page' => -1,
        //'services'    => $current_cat_ID,
        'post_status' => 'publish',
        'orderby' => 'date',
        'order' => 'DESC',
        'tax_query' => array(
            array(
                'taxonomy' => 'services',
                'field'    => 'id',
                'terms'    => array( $term_id ),

            )
        )
    );
    $posts = new WP_Query( $args_posts );
    if($posts->have_posts()){
        while ( $posts->have_posts() ) { $posts->the_post();
            $return['objects'][get_the_ID()] = get_the_title();
        }
    wp_reset_postdata();
    }

    wp_send_json( $return );
    exit();
}

add_action('wp_ajax_get_object', 'get_object_form');
add_action('wp_ajax_nopriv_get_object', 'get_object_form');

function get_option_form(){
    
    $return = [];
    $return['status'] = false;

    $object_id = isset($_POST['object_id']) ? (int)$_POST['object_id']  : false;
    $return['object_id'] = $object_id;
    $return['params'] = [];

    $params = get_field('params',$object_id);
    foreach($params as $key => $item){
        $return['params'][$key] = __($item['title']);
    }

    wp_send_json( $return );
    exit();
    
}

add_action('wp_ajax_get_option', 'get_option_form');
add_action('wp_ajax_nopriv_get_option', 'get_option_form');



function get_service_price(){
    
    $return = [];
    $return['status'] = false;

    $post_id = isset($_POST['post_id']) ? (int)$_POST['post_id']  : false;
    $return['post_id'] = $post_id;
    $return['html'] = '';


    $prices = get_field('_prices',$post_id); 

    ob_start();
    
    foreach($prices as $item){ ?>
    
    
    <div class="prise-list-table">
        <div class="table-name"><?=__($item['name'])?></div>
        <?=__($item['table'])?>

    </div>
    <div class="prise-list-btn">
        <a data-toggle="modal" href="#popups-application-get-price"><?php the_field('lng_get_get','option') ?></a>
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21" height="21" viewBox="0 0 21 21"><g><g transform="translate(-894 -1448)"><image width="21" height="21" transform="translate(894 1448)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAA8klEQVQ4T+3UPUoDQRQA4C/YewQjGIKkygm0sxIhrfdIb+0BPEAKaxFsLbyA6AkExcpCPYE82JE47k9m3dLX7TLz7Zt57+3IsHGC5WhAM8BLnA6FfoO4DvQOO4UZ32NR7fkBxrtA3yNlvBbAb3jBL3AdneOpAI2lteBf0BzcxmdKKh2/JNMc3I/iYNoXrTtyJHSF3T5o0x22ons4wzE+sqI1FgWt6DNWmOBoDW4D49udx9/K4MM0elUx6rquE41NCY7F42oworpNsRGa4HPc4qZjKDZGS4brH62fqGj4+JX1iRku8jF9qFqnD5j2POIgPXwBvgRUJ6JXPgEAAAAASUVORK5CYII="/></g></g></svg>
    </div>
    <?php }

    $return['html'] = ob_get_contents();
    ob_end_clean();

    wp_send_json( $return );
    exit();
    
}

add_action('wp_ajax_get_service_price', 'get_service_price');
add_action('wp_ajax_nopriv_get_service_price', 'get_service_price');



add_action('admin_menu', 'remove_admin_menu');
function remove_admin_menu() {
    remove_submenu_page( 'index.php', 'update-core.php' );
   // remove_menu_page('plugins.php');
    remove_menu_page('edit.php?post_type=acf-field-group');

}
function del_upgrade_nag() {
    echo '<style type="text/css">
            .update-nag , #wp-admin-bar-updates {display: none}
          </style>';
}
add_action('admin_head', 'del_upgrade_nag');



function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    remove_action( 'wp_head', 'feed_links_extra', 3 ); 
    remove_action( 'wp_head', 'feed_links',       2 ); 

    remove_action( 'wp_head', 'rsd_link'            ); 
    remove_action( 'wp_head', 'wlwmanifest_link'    ); 
    add_filter('the_generator', '__return_empty_string'); 
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );
    remove_action( 'wp_head', 'wp_resource_hints', 2);
}
if ( !is_admin() ) {
    add_action( 'init', 'disable_emojis' );
}


function wpbeginner_numeric_posts_nav() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 ){
        

        for($i = 1; $i <= $max; $i++){
            $links[] = $i;
        
        }
    }

    
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
     //   $links[] = $paged - 1;
      //  $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
      //  $links[] = $paged + 2;
      //  $links[] = $paged + 1;
    }
 
    echo '<div class="pagination"><ul>' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() ){
        // printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
    }
        
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) ){
           // echo '<li>…</li>';
        }
            
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( ( ($paged == $link) ? "#" : get_pagenum_link( $link )) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) ){
           //  echo '<li>…</li>' . "\n";
        }
           
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() ){
       //  printf( '<li>%s</li>' . "\n", get_next_posts_link() );
    }
       
 
    echo '</ul></div>' . "\n";
 
}


function kama_excerpt( $args = '' ){
	global $post;

	$default = array(
		'maxchar'   => 180,   // количество символов.
		'text'      => '',    // какой текст обрезать (по умолчанию post_excerpt, если нет post_content.
							  // Если есть тег <!--more-->, то maxchar игнорируется и берется все до <!--more--> вместе с HTML
		'autop'     => true,  // Заменить переносы строк на <p> и <br> или нет
		'save_tags' => '',    // Теги, которые нужно оставить в тексте, например '<strong><b><a>'
		'more_text' => '...', // текст ссылки читать дальше
	);

	if( is_array($args) ) $_args = $args;
	else                  parse_str( $args, $_args );

	$rg = (object) array_merge( $default, $_args );
	if( ! $rg->text ) $rg->text = $post->post_excerpt ?: $post->post_content;
	$rg = apply_filters( 'kama_excerpt_args', $rg );

	$text = $rg->text;
	$text = preg_replace( '~\[([a-z0-9_-]+)[^\]]*\](?!\().*?\[/\1\]~is', '', $text ); // убираем блочные шорткоды: [foo]some data[/foo]. Учитывает markdown
	$text = preg_replace( '~\[/?[^\]]*\](?!\()~', '', $text ); // убираем шоткоды: [singlepic id=3]. Учитывает markdown
	$text = trim( $text );

	// <!--more-->
	if( strpos( $text, '<!--more-->') ){
		preg_match('/(.*)<!--more-->/s', $text, $mm );

		$text = trim($mm[1]);

		$text_append = ' <a href="'. get_permalink( $post->ID ) .'#more-'. $post->ID .'">'. $rg->more_text .'</a>';
	}
	// text, excerpt, content
	else {
		$text = trim( strip_tags($text, $rg->save_tags) );

		// Обрезаем
		if( mb_strlen($text) > $rg->maxchar ){
			$text = mb_substr( $text, 0, $rg->maxchar );
			$text = preg_replace('~(.*)\s[^\s]*$~s', '\\1 ...', $text ); // убираем последнее слово, оно 99% неполное
		}
	}

	// Сохраняем переносы строк. Упрощенный аналог wpautop()
	if( $rg->autop ){
		$text = preg_replace(
			array("~\r~", "~\n{2,}~", "~\n~",   '~</p><br ?/>~'),
			array('',     '</p><p>',  '<br />', '</p>'),
			$text
		);
	}

	$text = apply_filters( 'kama_excerpt', $text, $rg );

	if( isset($text_append) ) $text .= $text_append;

	return ($rg->autop && $text) ? "<p>$text</p>" : $text;
}

