<?php get_header(); ?>

    <div class="panel-protection">
        <div class="page-title"><?php single_term_title(); ?></div>
        <div class="line-27"></div>

<?php
    $term = get_queried_object();

    $current_cat_ID = @$term->term_id;
    
    $args_posts = array(
        'post_type' => 'service',
        'posts_per_page' => -1,
        //'services'    => $current_cat_ID,
        'post_status' => 'publish',
        'orderby' => 'date',
        'order' => 'DESC',
        'tax_query' => array(
            array(
                'taxonomy' => 'services',
                'field'    => 'id',
                'terms'    => array( $current_cat_ID ),

            )
        )
    );
    $posts = new WP_Query( $args_posts );

 ?>

        <?php if($posts->have_posts()){ ?>

        <?php $mass_one = array(); ?>
        <?php $mass_two = array(); ?>
        <?php $count_posts = 1; ?>

        <?php while ( $posts->have_posts() ) { $posts->the_post(); 

                $icon = false;
                $image = false;

                $get_icon = get_field('icon');
                if($get_icon != '0'){
                    $icon = $get_icon;
                }

                $image_1 = wp_get_attachment_image_url( get_field('image_1'), 'orland-banner_clients' );
                $image_2 = wp_get_attachment_image_url( get_field('image_1_hover'), 'orland-banner_clients' );

                if(!$image_2){ $image_2 = $image_1; }

                $image[0] = $image_1;
                $image[1] = $image_2;
                
                if($count_posts <= 13){
                    $mass_one[] = array(
                        'title' => get_the_title(),
                        'icon' => $icon,
                        'href' => get_the_permalink(),
                        'image' => $image,
                    );
                } else {
                    
                    $mass_two[] = array(
                        'title' => get_the_title(),
                        'icon' => $icon,
                        'href' => get_the_permalink(),
                        'image' => $image,
                    );

                }
            $count_posts++;
            ?>
        
        <?php } wp_reset_postdata();  ?>

            
        <div class="container-full">
            
            <ul class="panel-protection-list">
                <?php if($mass_one){ ?>
                <?php foreach($mass_one as $item){ ?>
                <li>
                    <a href="<?=$item['href']?>">
                        <span class="panel-protection-list-img">
                            <?php if($item['icon']){ ?>
                            <i class="icon-<?=$item['icon']?>"></i>
                            <?php } else { ?>
                                
                            <span class="panel-protection-list-img">
                                <span class="panel-protection-list-img-normal"><img src="<?=@$item['image'][0]?>" alt=""></span>
                                <span class="panel-protection-list-img-hover"><img src="<?=@$item['image'][1]?>" alt=""></span>
                            </span>
                            
                           <?php } ?>

                        </span>
                        <span class="panel-protection-list-name"><?=$item['title']?></span>
                    </a>
                </li>
                <?php } } ?>

            </ul>
        </div>
        <div class="action-baners">
            <div class="container-full">

                <?php $banner_s = get_field('banner_s','services_'.$current_cat_ID); ?>
                <?php if($banner_s){ ?>
                <?php foreach($banner_s as $banner){ ?>
                
                <div class="action-baner">
                    <a href="<?=__($banner['url'])?>">
                        <img src="<?= wp_get_attachment_image_url( @$banner['image'], 'orland-banner_clients' )?>" alt="">
                        <div class="action-baner-content">
                            <div class="action-baner-title"><?=__($banner['title_1'])?></div>
                            <div class="action-baner-name"><?=__($banner['title_2'])?></div>
                        </div>
                    </a>
                </div>
                <?php } ?>
                <?php } ?>
            </div>
        </div>
    <?php if($mass_two){ ?>
        <div class="container-full">
            <ul class="panel-protection-list">
                <?php foreach($mass_two as $item){ ?>
                <li>
                    <a href="<?=$item['href']?>">
                        <span class="panel-protection-list-img">
                            <?php if($item['icon']){ ?>
                            <i class="icon-<?=$item['icon']?>"></i>
                            <?php } else { ?>
                                
                            <span class="panel-protection-list-img">
                                <span class="panel-protection-list-img-normal"><img src="<?=@$item['image'][0]?>" alt=""></span>
                                <span class="panel-protection-list-img-hover"><img src="<?=@$item['image'][1]?>" alt=""></span>
                            </span>
                            
                           <?php } ?>

                        </span>
                        <span class="panel-protection-list-name"><?=$item['title']?></span>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
        
        <?php } else { ?>
            
        <?php } ?>
        
        <div class="page-title fs-30"><?php the_field('title_2','services_'.$current_cat_ID) ?></div>
        <div class="line-27"></div>
        <div class="panel-protection-text"><?php the_field('description_2','services_'.$current_cat_ID) ?></div>
    </div>

    <?php get_footer(); ?>
