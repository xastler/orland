$(document).ready(function () {
    $('._form_add_history button.btn_content').click(function(){

    });

    if(document.location.hash){
        setTimeout(function() {
            $('[data-click="'+document.location.hash+'"]').click();
        }, 500);
    }


function select_shange(){
    $('.select').each(function(){
        // Variables
        var $this = $(this),
            selectOption = $this.find('option'),
            selectOptionLength = selectOption.length,
            selectedOption = selectOption.filter(':selected'),
            dur = 500;
        var select= $(this).find('option').filter(':selected').html();
        $this.hide();
        // Wrap all in select box
        $this.wrap('<div class="select"></div>');
        // Style box
        $('<div>',{
            class: 'select__gap',
            text: select
        }).insertAfter($this);

        var selectGap = $this.next('.select__gap'),
            caret = selectGap.find('.caret');
        // Add ul list
        $('<ul>',{
            class: 'select__list'
        }).insertAfter(selectGap);

        var selectList = selectGap.next('.select__list');
        // Add li - option items
        for(var i = 0; i < selectOptionLength; i++){
            $('<li>',{
                class: 'select__item',
                html: $('<span>',{
                    text: selectOption.eq(i).text()
                })
            })
                .attr('data-value', selectOption.eq(i).val())
                .appendTo(selectList);
        }
        // Find all items
        var selectItem = selectList.find('li');

        selectList.slideUp(0);
        selectGap.on('click', function(){
            if(!$(this).hasClass('on')){
                $(this).addClass('on');
                selectList.slideDown(dur);

                selectItem.on('click', function(){
                    var chooseItem = $(this).data('value');
                    
                    var select = $(this).parents('.select').find('select');


                    $(select).val(chooseItem).attr('selected', 'selected');

                    if($(this).parents('._container_cost')){
                        if($(select).hasClass('_service_service_change')){
                            
                            $.ajax({
                                url: '/wp-admin/admin-ajax.php',
                                data: 'action=get_object&term_id='+chooseItem,
                                type:'POST',
                                beforeSend: function( xhr ) { $('._add_mask_cl').addClass('_mask'); },
                                success:function(data){
                                    if(data.objects){
                                        var vals = '';
                                        for(i in data.objects){
                                            vals += '<option value="'+i+'">'+data.objects[i]+'</option>';
                                        }
                                        $('select._service_object_change').html(vals);
                                        select_shange();
                                    }

                                        $('.find-cost-lvl.form-lvl-1').removeClass('active');
                                        $('.find-cost-lvl.form-lvl-1').removeClass('form-start');
                                        $('.find-cost-lvl.form-lvl-2').addClass('active');
                                        setTimeout(function(){
                                            $('.find-cost-lvl.form-lvl-1').addClass('find-cost-back');
                                        }, 500);
                                        $('._add_mask_cl').removeClass('_mask');
                                }
                            });
                        }
                        
                        if($(select).hasClass('_service_object_change')){
                            $.ajax({
                                url: '/wp-admin/admin-ajax.php',
                                data: 'action=get_option&object_id='+chooseItem,
                                type:'POST',
                                beforeSend: function( xhr ) { $('._add_mask_cl').addClass('_mask'); },
                                success:function(data){
                                    if(data.params){
                                        var vals = '';
                                        for(i in data.params){
                                            vals += '<option value="'+i+'">'+data.params[i]+'</option>';
                                        }
                                        $('select._service_options_change').html(vals);
                                        select_shange();
                                              //  $('.find-cost-lvl.form-lvl-2').removeClass('active');
                                              //  $('.find-cost-lvl.form-lvl-3').addClass('active');
                                    }

                                    $('.find-cost-lvl.form-lvl-2').removeClass('active');
                                    $('.find-cost-lvl.form-lvl-3').addClass('active');
                                    setTimeout(function(){
                                        $('.find-cost-lvl.form-lvl-2').addClass('find-cost-back');
                                    }, 500);
                                    $('._add_mask_cl').removeClass('_mask');
                                }
                            });
                        }
                        if($(select).hasClass('_service_options_change')){
                        
                        }
                        
                        if($(select).hasClass('_get_service')){
                            if(Number(chooseItem) > 0){
                                $.ajax({
                                    url: '/wp-admin/admin-ajax.php',
                                    data: 'action=get_service_price&post_id='+chooseItem,
                                    type:'POST',
                                    beforeSend: function( xhr ) {},
                                    success:function(data){
                                        if(data.html){
                                            $(select).parents('._tab_cntn').find('._price_list_content').html(data.html);
                                        }
                                        
                                    //    console.log(data);
                                    }
                                });
                            }
                            
                        }

                        $('input._service_service_change').val($('select._service_service_change option:selected').text());
                        $('input._service_object_change').val($('select._service_object_change option:selected').text());
                        $('input._service_options_change').val($('select._service_options_change option:selected').text());

                      //  console.log($('select._service_service_change option:selected').text());
                       // console.log($('select._service_object_change option:selected').text());
                       // console.log($('select._service_options_change option:selected').text());
                        
                    }
                    
                    selectGap.text($(this).find('span').text());

                    selectList.slideUp(dur);
                    selectGap.removeClass('on');
                });

            } else {
                $(this).removeClass('on');
                selectList.slideUp(dur);
            }
        });

    });
}
select_shange();
        //Home FORM find-cost
    $('.find-cost-lvl.form-start .select__gap').click(function () {
        $('.find-cost-main').addClass('no-active');
        $('.find-cost-lvl.form-start').addClass('active');
    });
    $('.find-cost-lvl.form-start .select__item').click(function () {
      
    });
    $(document).on(' click','.find-cost-lvl.form-lvl-2 .select__item',function(){
   // $('.find-cost-lvl.form-lvl-2 .select__item').click(function () {
       
    });
    $(document).on(' click','.find-cost-lvl.form-lvl-3 .select__item',function(){
    //$('.find-cost-lvl.form-lvl-3 .select__item').click(function () {
        $('.find-cost-lvl.form-lvl-3').removeClass('active');
        $('.find-cost-lvl').addClass('no-active');
        $('.find-cost-form').addClass('active');
        $('.find-cost-main').addClass('active');
        setTimeout(function(){
            $('.find-cost').addClass('form-back');
        }, 1550);
    });
    //End-Home FORM find-cost

    

});

function share_fb(url){
    var href  = 'https://www.facebook.com/sharer.php?u=';
    href  += url;
    window.open(href,'','toolbar=0,status=0,width=626,height=436');
}

