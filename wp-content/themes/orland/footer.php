



    </div>
    <footer class="home-footer parallax-window" >

        <div class="parallax-footer-logo"><img src="<?=get_template_directory_uri()?>/orland/build/img/footer-logo.png" alt=""></div>
        <div class="container-content">
            <div class="home-footer-left">
                <div class="home-footer-block">
                    <div class="home-footer-title"><?php the_field('lng_services','option') ?></div>

                    <?php $menu_services_arr = wp_get_nav_menu_items( 10);
                    $menu_services = array();
                    foreach($menu_services_arr as $item){
                        if($item->menu_item_parent){
                            $menu_services[$item->menu_item_parent]['child'][$item->ID]['title'] = __($item->title);
                            $menu_services[$item->menu_item_parent]['child'][$item->ID]['href'] = $item->url;
                        } else {
                            $menu_services[$item->ID]['title'] = __($item->title);
                            $menu_services[$item->ID]['href'] = $item->url;
                        }
                    }
                    ?>

                    <?php if($menu_services){ ?>
                        <?php foreach($menu_services as $menu){ ?>
                            <div class="home-footer-dop_list">
                                <p><a href="<?=$menu['href']?>"><?=$menu['title']?></a></p>
                                <?php if(isset($menu['child'])){ ?>
                                    <ul>
                                        <?php foreach($menu['child'] as $child){ ?>
                                            <li><a href="<?=$child['href']?>"><?=$child['title']?></a></li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="home-footer-block">
                    <div class="obg-block">
                        <div class="home-footer-title"><?php the_field('lng_about_company','option') ?></div>

                        <?php wp_nav_menu( array(
                            'theme_location'  => 'footer_company',
                            'container'       => 0,
                            'menu_class'      => 'home-footer-list',
                            'echo'            => true,
                            'fallback_cb'     => 'wp_page_menu',
                            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',

                        ) );
                        ?>
                    </div>
                </div>
            </div>
            <!--<div class="home-footer-logo">-->
            <!--<a href="index.html"> <img src="./img/footer-logo.png" alt="logo"></a>-->
            <!--</div>-->
            <div class="home-footer-right">
                <div class="home-footer-block">
                    <div class="home-footer-title"><?php the_field('lng_clients','option') ?></div>

                    <?php wp_nav_menu( array(
                        'theme_location'  => 'footer_klients',
                        'container'       => 0,
                        'menu_class'      => 'home-footer-list',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',

                    ) );
                    ?>
                </div>
                <div class="home-footer-block">
                    <div class="home-footer-contact">
                        <div class="home-footer-title"><?php the_field('lng_contacts','option') ?></div>
                        <div class="home-footer-phone">
                            <i class="icon-call-answer"></i>
                            <?php $footer_phones = get_field('footer_phones','option'); ?>
                            <?php if($footer_phones){ ?>
                                <?php foreach($footer_phones as $header_phone){ ?>
                                    <p><a href="tel:<?= str_replace(['-',' ',')','('],'',__($header_phone['pre']).__($header_phone['tel'])); ?>"><span><?php _e($header_phone['pre']); ?> </span><?php _e($header_phone['tel']); ?></a></p>
                                <?php } ?>
                            <?php } ?>

                        </div>
                        <div class="home-footer-address">
                            <i><img src="<?=get_template_directory_uri()?>/orland/build/img/iconef.png" alt=""></i>
                            <p><?php the_field('footer_adress','option') ?></p>
                        </div>
                    </div>
                    <div class="home-footer-social">
                        <p><?php the_field('lng_social','option') ?> </p>

                        <a href="<?php the_field('footer_href_fb','option') ?>" target="_blank">
                            <i class="icon-facebook-logo-in-circular-button-outlined-social-symbol"></i>
                        </a>
                        <a href="<?php the_field('footer_gp','option') ?>" target="_blank">
                            <i class="icon-google-plus-circular-button"></i>
                        </a>

                    </div>
                    <div class="home-footer-QR">
                        <p><?php the_field('lng_qr','option') ?></p>
                        <?php if(get_field('footer_qr','option') ){ ?>
                            <img src="<?= wp_get_attachment_image_url( @get_field('footer_qr','option'), 'orland-gallery' )?>" alt="">
                        <?php } else { ?>
                            <img src="<?=get_template_directory_uri()?>/orland/build/img/footer-QR.png" alt="">
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="copyrighted">
        <div class="container-full">
            <div class="copyrighted-left"><?php the_field('lng_copyrighted','option') ?></div>
            <div class="copyrighted-right">Crieted by <span>INDIGO</span></div>
        </div>
    </div>
    <div class="footer-bg"><img src="<?=get_template_directory_uri()?>/orland/build/img/bg-footer.jpg" alt=""></div>
<div class="modal fade" id="popups-galery" tabindex="-1" role="dialog" aria-labelledby="popups-galery-Label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <!--<div class="modal-header">-->
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon-close"></span></button>-->
                <!--</div>-->
                <div class="modal-body">

                </div>
        </div>
    </div>
</div>
<!-- END wrapper-->
<div class="modal fade" id="popups-call-back" tabindex="-1" role="dialog" aria-labelledby="popups-call-back-Label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon-close"></span></button>
                    <h4 class="modal-title" id="popups-call-back-Label"><?php the_field('lng_get_tel','option') ?></h4>
                <img src="<?=get_template_directory_uri()?>/orland/build/img/footer-logo.png" alt="">
                </div>
                <?php if(qtranxf_getLanguage() == 'en'){ ?>
                    <?php _e(do_shortcode('[contact-form-7 id="453" title="Заказать звонок_en"]')); ?>
                <?php } else { ?>
                    <?php _e(do_shortcode('[contact-form-7 id="294" title="Заказать звонок"]')); ?>
                <?php } ?>

                
                
        </div>
    </div>
</div>


<div class="modal fade" id="popups-application-get-price" tabindex="-1" role="dialog" aria-labelledby="popups-application-get-price-Label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon-close"></span></button>
                    <h4 class="modal-title" id="popups-application-get-price-Label"><?php the_field('lng_get_get','option') ?></h4>
                <img src="<?=get_template_directory_uri()?>/orland/build/img/footer-logo.png" alt="">
                </div>
                <?php if(qtranxf_getLanguage() == 'en'){ ?>
                    <?php _e(do_shortcode('[contact-form-7 id="453" title="Заказать звонок_en"]')); ?>
                <?php } else { ?>
                    <?php _e(do_shortcode('[contact-form-7 id="294" title="Заказать звонок"]')); ?>
                <?php } ?>

                
                
        </div>
    </div>
</div>




<div class="modal fade" id="popups-application" tabindex="-1" role="dialog" aria-labelledby="popups-application-Label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon-close"></span></button>
                    <h4 class="modal-title" id="popups-application-Label"><?php the_field('lng_get_message','option') ?></h4>
                 <img src="<?=get_template_directory_uri()?>/orland/build/img/footer-logo.png" alt="">
                </div>

                <?php if(qtranxf_getLanguage() == 'en'){ ?>
                    <?php _e(do_shortcode('[contact-form-7 id="454" title="Оставить заявку_en"]')); ?>
                <?php } else { ?>
                    <?php _e(do_shortcode('[contact-form-7 id="297" title="Оставить заявку"]')); ?>
                <?php } ?>

                

<?php if(0){ ?>
            <form action="POST">
               
                <div class="modal-body">
                    <div class="block-input">
                        <input type="text" name="name" placeholder="Ф.И.О" required>
                    </div>
                    <div class="block-input">
                        <input type="text" name="phone" placeholder="Ваш телефон" required  >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit">Отправить</button>
                </div>
                
            </form>
<?php } ?>

        </div>
    </div>
</div>


<?php wp_footer(); ?>

</body>
</html>



