if ($(window).width() >= '1700'){
    var size = $(window).width();
    var re_size = 100 + ((size - 1700)/15);
    $('body').css('zoom', re_size + '%');
} else {
    $('body').css('zoom', '100%');
}
$( window ).resize(function() {
    if ($(window).width() >= '1700'){
        var size = $(window).width();
        var re_size = 100 + ((size - 1700)/15);
        $('body').css('zoom', re_size + '%');
    } else {
        $('body').css('zoom', '100%');
    }
});

$(".file-res").append("<span class='text'>Файл не выбран</span>");
$('.checkbox-enter .wpcf7-list-item input').after($('<label></label>'));

$( document ).ready(function() {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    $("._vac_file").change(function(){
        var filename = $(this).val().replace(/.*\\/, "");
        $(".file-res .text").html(filename);
    });
    window.onscroll = function(){
        if ($(document).scrollTop() + $(window).height() > $('.copyrighted').offset().top && $(document).scrollTop() - $('.home-footer').offset().top < $('.home-footer').height()){
            $('.home-footer').addClass('active');
        }else{
            $('.home-footer').removeClass('active');

        }
    }
    $(function(){
        $('.phone').mask("38 (999) 999-9999");
    });

    $('.one-sliders').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        arrows:false,
        // autoplay: true
    });
    $('.home-reviews-sliser').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        adaptiveHeight: true,
        arrows:false,
        autoplay: true,
        responsive: [
            {
                breakpoint: 1550,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 1050,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $('.parallax').parallax();

    $(".one-sliders").on('afterChange', function(event, slick, currentSlide){
        $(".one-sliders-num span").text('0'+ (currentSlide + 1));
    });

    $( function() {
        $("#home-tabs" ).tabs();
        $("#prise-list-tabs" ).tabs();
        $("#page-galery-tabs").tabs();
        $("#for-clients-tabs").tabs();
    } );

    $('.header-navigation-btn').click(function () {
        $('body').addClass('active');
        $('.wrapper').addClass('active');
        $('.panel-menu').addClass('active');
    });

    //START apartment-alarm-home
        $('.apartment-alarm-home_img').click(function () {
            $('.apartment-alarm-home_img span').removeClass('active-point');
        });

        $('.apartment-alarm-home_info li span').click(function () {
            $('.apartment-alarm-home_img span').removeClass('active-point');
            $('.apartment-alarm-home_img span.' + $(this).data('href')).addClass('active-point');
        });

        $('.apartment-alarm-home_info li span').hover(function () {
            $('.apartment-alarm-home_img span.' + $(this).data('href')).hover();
        });
    //END apartment-alarm-home


    $('.panel-menu-close').click(function () {
        $('.panel-menu').removeClass('active');
        $('.wrapper').removeClass('active');
        $('body').removeClass('active');
    });

    $('.page-galery-list li').click(function () {
        if($(this).data('img')!=''&&$(this).data('img')!=' '){
            $('#popups-galery .modal-body').html('<img src="'+ $(this).data('img') + '">');
        }else{

            $('#popups-galery .modal-body').html(' <iframe class="embed-responsive-item" src="'+ $(this).data('video') +'" allowfullscreen></iframe>');
        }
        $(document).mousedown((e) => {
            const elem = $('#popups-galery .modal-dialog');
            if (elem.has(e.target).length === 0) {
                $('#popups-galery .modal-body').html('');
            }
        });
    });

     $('.select').each(function(){
        // Variables
        var $this = $(this),
            selectOption = $this.find('option'),
            selectOptionLength = selectOption.length,
            selectedOption = selectOption.filter(':selected'),
            dur = 500;
        var select= $(this).find('option').filter(':selected').html();
        $this.hide();
        // Wrap all in select box
        $this.wrap('<div class="select"></div>');
        // Style box
        $('<div>',{
            class: 'select__gap',
            text: select
        }).insertAfter($this);

        var selectGap = $this.next('.select__gap'),
            caret = selectGap.find('.caret');
        // Add ul list
        $('<ul>',{
            class: 'select__list'
        }).insertAfter(selectGap);

        var selectList = selectGap.next('.select__list');
        // Add li - option items
        for(var i = 0; i < selectOptionLength; i++){
            $('<li>',{
                class: 'select__item',
                html: $('<span>',{
                    text: selectOption.eq(i).text()
                })
            })
                .attr('data-value', selectOption.eq(i).val())
                .appendTo(selectList);
        }
        // Find all items
        var selectItem = selectList.find('li');

        selectList.slideUp(0);
        selectGap.on('click', function(){
            if(!$(this).hasClass('on')){
                $(this).addClass('on');
                selectList.slideDown(dur);

                selectItem.on('click', function(){
                    var chooseItem = $(this).data('value');

                    $('select').val(chooseItem).attr('selected', 'selected');
                    selectGap.text($(this).find('span').text());

                    selectList.slideUp(dur);
                    selectGap.removeClass('on');
                });

            } else {
                $(this).removeClass('on');
                selectList.slideUp(dur);
            }
        });

    });

    //Home FORM find-cost

    $('.find-cost-lvl.form-lvl-1 .select__item').click(function () {
        $('.find-cost-lvl.form-lvl-1').removeClass('active');
        $('.find-cost-lvl.form-lvl-1').removeClass('form-start');
        $('.find-cost-lvl.form-lvl-2').addClass('active');
        setTimeout(function(){
            $('.find-cost-lvl.form-lvl-1').addClass('find-cost-back');
        }, 500);
    });

    $('.find-cost-lvl.form-lvl-2 .select__item').click(function () {
        $('.find-cost-lvl.form-lvl-2').removeClass('active');
        $('.find-cost-lvl.form-lvl-3').addClass('active');
        setTimeout(function(){
            $('.find-cost-lvl.form-lvl-2').addClass('find-cost-back');
        }, 500);
    });
    $('.find-cost-lvl.form-lvl-3 .select__item').click(function () {
        $('.find-cost-lvl.form-lvl-3').removeClass('active');
        $('.find-cost-lvl').addClass('no-active');
        $('.find-cost-form').addClass('active');
        $('.find-cost-main').addClass('active');
        setTimeout(function(){
            $('.find-cost').addClass('form-back');
        }, 1550);

    });

    $('.form-lvl-1').click(function () {
        if($(this).hasClass('find-cost-back')){
            $('.find-cost-lvl').removeClass('active');
            $('.find-cost-lvl.form-lvl-1').addClass('active');
            $('.find-cost-lvl.form-lvl-1').addClass('form-start');
            $('.find-cost-lvl').removeClass('find-cost-back');
        }
    });
    $('.form-lvl-2').click(function () {
        if($(this).hasClass('find-cost-back')){
            $('.find-cost-lvl.form-lvl-2').addClass('active');
            $('.find-cost-lvl.form-lvl-3').removeClass('active');
            $(this).removeClass('find-cost-back');
        }
    });
    $('.find-cost-main').click(function () {
        $(this).removeClass('active');
        $('.find-cost-lvl .select__gap.on').click();
        $('.find-cost-form').removeClass('active');
        $('.find-cost-lvl').removeClass('no-active');
        $('.find-cost-lvl').removeClass('active');
        $('.find-cost-lvl.form-lvl-1').addClass('active');
        $('.find-cost-lvl').removeClass('find-cost-back');
        setTimeout(function(){

            $('.find-cost').removeClass('form-back');
        }, 1500);
    });


    //End-Home FORM find-cost


    //Contact open & init-map
        $('.page-promotions-city-name').click(function () {
            if(!$(this).parent().hasClass('active')){
                $('.page-promotions-city-block').removeClass('active');
                $('.page-promotions-city-info').slideUp();
                $(this).parent().addClass('active');
                $(this).next('div').slideToggle();
                if($('#map').length){
                    init($(this).next('div').find('.page-promotions-city-address').data('point-x'),$(this).next('div').find('.page-promotions-city-address').data('point-y'),$(this).next('div').find('.page-promotions-city-address').data('gps-img'),$(this).next('div').find('.page-promotions-city-address').data('zoom'));
                }
            }else if($(this).parent().hasClass('active')){
                $('.page-promotions-city-block').removeClass('active');
                $('.page-promotions-city-info').slideUp();
            }
        });

        $('.page-promotions-city-block .page-promotions-city-address').click(function () {
            if($('#map').length){
                init($(this).data('point-x'),$(this).data('point-y'),$(this).data('gps-img'),$(this).data('zoom'));
            }
        });

        if($('#map').length){
            var elem='.page-promotions-city-block.active .page-promotions-city-name';
            init($(elem).next('div').find('.page-promotions-city-address').data('point-x'),$(elem).next('div').find('.page-promotions-city-address').data('point-y'),$(elem).next('div').find('.page-promotions-city-address').data('gps-img'),$(elem).next('div').find('.page-promotions-city-address').data('zoom'));
        }

        function init(point_x,point_y,gps_img,zoom) {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: zoom,

                // The latitude and longitude to center the map (always required)
                //center: new google.maps.LatLng(48.75895, 8.24215),
                center: new google.maps.LatLng(point_x, point_y),

                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]}, {"elementType": "labels.text.stroke", "stylers": [{"color": "#f5f5f5"}]}, {"featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{"color": "#bdbdbd"}]}, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]}, {"featureType": "poi", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]}, {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]}, {"featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}, {"featureType": "road", "elementType": "geometry", "stylers": [{"color": "#ffffff"}]}, {"featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]}, {"featureType": "road.highway", "elementType": "geometry", "stylers": [{"color": "#dadada"}]}, {"featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]}, {"featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}, {"featureType": "transit.line", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]}, {"featureType": "transit.station", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]}, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#c9c9c9"}]}, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}]
            };

            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            var markerImage = new google.maps.MarkerImage(
                gps_img,
                new google.maps.Size(68,92),
            );

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                icon: markerImage,
                position: new google.maps.LatLng(point_x, point_y),
                map: map,
                title: 'Augusta Apartments\n' +
                'Eichstrasse 6\n' +
                '76530 Baden-Baden'
            });
        }
    //End-Contact open & init-map


    $('.alarm-apartment-open').click(function () {
        $(this).toggleClass('active');
        $('.alarm-apartment-text').toggleClass('active');

    });



    // When the window has finished loading create our google map below
    //google.maps.event.addDomListener(window, 'load', init);

});






$('#tabs-1 .home-tab-slicks').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 6,
    adaptiveHeight: true,
    arrows:false,
    autoplay: true,
    responsive: [
        {
            breakpoint: 1600,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 5
            }
        },
        {
            breakpoint: 1366,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 1100,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 850,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
$('#tabs-2 .home-tab-slicks').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 6,
    adaptiveHeight: true,
    arrows:false,
    autoplay: true,
    responsive: [
        {
            breakpoint: 1600,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 5
            }
        },
        {
            breakpoint: 1366,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 1100,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 850,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
$('#tabs-3 .home-tab-slicks').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 6,
    adaptiveHeight: true,
    arrows:false,
    autoplay: true,
    responsive: [
        {
            breakpoint: 1600,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 5
            }
        },
        {
            breakpoint: 1366,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 1100,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 850,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
$('#tabs-4 .home-tab-slicks').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 6,
    adaptiveHeight: true,
    arrows:false,
    autoplay: true,
    responsive: [
        {
            breakpoint: 1600,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 5
            }
        },
        {
            breakpoint: 1366,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 1100,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 850,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
$('#tabs-5 .home-tab-slicks').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 6,
    adaptiveHeight: true,
    arrows:false,
    autoplay: true,
    responsive: [
        {
            breakpoint: 1600,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 5
            }
        },
        {
            breakpoint: 1366,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 1100,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 850,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
$('#tabs-6 .home-tab-slicks').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 6,
    adaptiveHeight: true,
    arrows:false,
    autoplay: true,
    responsive: [
        {
            breakpoint: 1600,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 5
            }
        },
        {
            breakpoint: 1366,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 1100,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 850,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

